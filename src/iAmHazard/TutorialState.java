package iAmHazard;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

/**
 * TutorialState class
 * Displays a quick tutorial on how to play the game.
 */
public class TutorialState extends BasicGameState
{
    /* General */
    private StateBasedGame sbg;
	
	/* Images */
    private Image tutorialImage;
    private static final float TUTORIAL_IMAGE_X = 0;
    private static final float TUTORIAL_IMAGE_Y = 0;
    private static final String TUTORIAL_IMAGE = "images/state_tutorial/tutorial.png";
    private Image backButton;
    private static final float BACK_BUTTON_IMAGE_X = 630;
    private static final float BACK_BUTTON_IMAGE_Y = 10;
    private static final String BACK_BUTTON_IMAGE = "images/state_tutorial/back_button.png";
    private static final float IMAGE_SCALE = 0.64f;
    
    /**
	 * Gets the ID of MenuState
	 * @return ID of MenuState
	 */
    @Override
    public int getID()
    {
        return Constants.STATE_ID_TUTORIAL;
    }

    /**
	 * Initializes the game state
	 * @param gc GameContainer Object
	 * @param sbg StateBasedGame Object
	 * @throws SlickException
	 */
	@Override
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException
	{		
		this.sbg = sbg;
    	this.tutorialImage = new Image(TUTORIAL_IMAGE);
    	this.backButton = new Image(BACK_BUTTON_IMAGE);    
	}

	/**
	 * Renders the game state to the screen
	 * @param gc GameContainer Object
	 * @param sbg StateBasedGame Object
	 * @param gr Graphics Object
	 */
	@Override
    public void render(GameContainer gameContainer, StateBasedGame stateBasedGame, Graphics graphics) throws SlickException
    {
    	this.tutorialImage.draw(TUTORIAL_IMAGE_X, TUTORIAL_IMAGE_Y, IMAGE_SCALE);
    	this.backButton.draw(BACK_BUTTON_IMAGE_X, BACK_BUTTON_IMAGE_Y, IMAGE_SCALE);
    }

	/**
	 * Updates the game state
	 * @param gc GameContainer Object
	 * @param sbg StateBasedGame Object
	 * @param delta time passed since last frame (milliseconds)
	 */
	@Override
    public void update(GameContainer gameContainer, StateBasedGame stateBasedGame, int i) throws SlickException
    {
		
    }

	/**
	 * Handles the event when a mouse button is released (un-clicked)
	 * @param button denotes which mouse button is released
	 * @param x the x coordinate where the click was made 
	 * @param y the y coordinate where the click was made
	 */
	@Override
	public void mouseReleased(int button, int mx, int my) {
		/* Back */
		if ((mx >= BACK_BUTTON_IMAGE_X) && (mx <= BACK_BUTTON_IMAGE_X+this.backButton.getWidth()*IMAGE_SCALE) &&
			(my >= BACK_BUTTON_IMAGE_Y) && (my <= BACK_BUTTON_IMAGE_Y+this.backButton.getHeight()*IMAGE_SCALE)) {
				this.sbg.enterState(Constants.STATE_ID_MENU);
		}
	}
}