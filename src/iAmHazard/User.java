package iAmHazard;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * User class
 * Represents a user of the game (i.e. a gamer)
 */
public class User implements Comparable<User>, Serializable
{
	private static final long serialVersionUID = 3334979262340079354L;
	private int highscore;
	private String name;
	private ArrayList<Badge> badges;
	
	/**
	 * Constructor
	 * @param name username
	 */
	public User(String name)
	{
		this.highscore = 0;
		this.name = name;
		this.badges = new ArrayList<Badge>();
	}
	
	/**
	 * Gets the list of badges the user has achieved
	 * @return list of badges the user has achieved
	 */
	public ArrayList<Badge> getBadges()
	{
		return this.badges;
	}
	
	/**
	 * Gets the personal highscore of the user
	 * @return user's personal highscore
	 */
	public int getHighscore()
	{
		return this.highscore;
	}
	
	/**
	 * Gets the name of the user
	 * @return name of the user
	 */
	public String getName()
	{
		return this.name;
	}

	/**
	 * Adds a new Badge to the User. This method will attach the Badge object
	 * passed in as the parameter to the User. If the User already has the
	 * named Badge, this method will only increment the multiplicity of the
	 * respective Badge object.
	 * @param badge The new Badge to be added to the User's badges list
	 */
	public void addBadge(Badge badge)
	{
        System.out.println("addBadge()! :)");
		// Checks if the User already has the badge
		String newBadgeName = badge.getName();
		Iterator<Badge> badgeIter = this.badges.iterator();
		while (badgeIter.hasNext()) {
			Badge aBadge = badgeIter.next();
			if (newBadgeName.equals(aBadge.getName())) {
				// Increment the multiplicity of the Badge
                System.out.println("Incrementing badge mult...");
				aBadge.setMultiplicity(aBadge.getMultiplicity()+badge.getMultiplicity());
				return;
			}
		}
		
		this.badges.add(BadgeManager.copy(badge));
	}

	/**
	 * Updates the personal highscore of the user. It updates the highscore
	 * only if the new score is higher than the current highscore.
	 * @param highscore the new score
	 * @return True if the new score is higher than the current highscore.
	 */
	public boolean updateHighscore(int highscore)
	{
		if (this.highscore < highscore) {
			this.highscore = highscore;
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Comparison between a user's highscore and another user's highscore
	 * @param Object obj an object of type user  
	 * @return The difference between this user's highscore and the other user's highscore
	 */
	@Override
	public int compareTo(User otherUser)
	{
		return this.highscore - otherUser.getHighscore();
	}
}