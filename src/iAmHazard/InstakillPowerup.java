package iAmHazard;

/**
 * InstakillPowerup
 * Represents the powerup to instantly kill a Monster from the game
 */
public class InstakillPowerup extends Powerup
{
	private static final String DESCRIPTION = "KILLLLL!!!";
    private static final int DURATION = 1;
	private static final String POWERUP_IMAGE = "images/powerups/instakill.png";
	
	/**
	 * Constructor
	 * @param multiplicity powerup multiplicity
	 */
	public InstakillPowerup (int multiplicity)
	{
		super(DESCRIPTION, DURATION, POWERUP_IMAGE, multiplicity);
	}
	
	/**
	 * Kills the nearest monster to Player's position
	 * @param gameState the gameplay state
	 * @return True if successfully kills a monster
	 */
	@Override
	protected boolean doUpdate(GameplayState gps)
	{
		Monster nearestMonster = null;
		for (Monster monster : gps.getMonsters()) {
			if (monster.isExistent()) {
				if (nearestMonster == null) {
					nearestMonster = monster;
				} else if (monster.getXposition() < nearestMonster.getXposition()) {
					nearestMonster = monster;
				}
			}
		}
		
		if (nearestMonster != null) {
			nearestMonster.kill(false);
			return true;
		}
		return true;
	}
}