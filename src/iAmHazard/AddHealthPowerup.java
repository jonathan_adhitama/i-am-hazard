package iAmHazard;

/**
 * AddHealthPowerup class
 * Represents the powerup to add health to the Player
 */
public class AddHealthPowerup extends Powerup
{
	private static final String DESCRIPTION = "Healed!";
	private static final String IMAGE_LOCATION = "images/powerups/addHealth.png";
	private static final int DURATION = 1;
	private static final int ADDITIONAL_HEALTH = 1;
	
	/**
	 * Constructor
	 * @param multiplicity powerup multiplicity
	 */
	public AddHealthPowerup (int multiplicity)
	{
		super(DESCRIPTION, DURATION, IMAGE_LOCATION, multiplicity);
	}
	
	/**
	 * Adds a health to the player
	 * @param gameState the gameplay state
	 * @return True if successfully added player health
	 */
	@Override
	protected boolean doUpdate(GameplayState gameState)
	{
		if (gameState.getPlayer().getHealth() < gameState.getLevel().getHealth()) {
			int health = gameState.getPlayer().getHealth() + ADDITIONAL_HEALTH;
			gameState.getPlayer().setHealth(health);
			return true;
		}
		return false;
	}
}