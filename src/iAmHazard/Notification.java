package iAmHazard;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

/**
 * Notification class
 * Represents a notification object used to notify the Player during gameplay.
 */
public class Notification
{
    private static final float MESSAGE_OFFSET_X = 7;
    private static final float MESSAGE_OFFSET_Y = 3;
    private static final String NOTIFICATION_BG_IMAGE = "images/notifier/notif_rect.png";
    private String message;
    private int timeRemaining;
    private float xpos;
    private float ypos;
    
    private static final Color NOTIFICATION_FILL_COLOR = new Color(255,255,255,240);
    private static final int LEFT_RIGHT_BOX_MARGIN = 20;
    private static final int BOTTOM_UP_BOX_MARGIN = 10;
    private static final int BOX_ROUND_RADIUS = 7;
    
    /**
	 * Constructor
	 * @param message the message of the notification
	 * @param timeRemaining time remaining for the notification to be displayed on screen
	 * @param xpos x position of where to draw the notification
	 * @param ypos y position of where to draw the notification
	 */
    public Notification(String message, int timeRemaining, float xpos, float ypos)
    {
        this.message = message;
        this.timeRemaining = timeRemaining;
        this.xpos = xpos;
        this.ypos = ypos;
        try {
            new Image(NOTIFICATION_BG_IMAGE);
        } catch (Exception e) {
        }
    }
    
    /**
	 * Gets the message of the notification.
	 * @return the message of the notification
	 */
    public String getMessage()
    {
        return message;
    }

    /**
	 * Gets the time remaining for the notification to be displayed on screen
	 * @return timeRemaining time remaining for the notification to be displayed on screen
	 */
    public int getTimeRemaining()
    {
        return timeRemaining;
    }
    
    /**
	 * Sets the time remaining for the notification to be displayed on screen
	 * @param timeRemaining time remaining for the notification to be displayed on screen
	 */
    public void setTimeRemaining(int timeRemaining)
    {
        this.timeRemaining = timeRemaining;
    }

    /**
	 * Gets the x position of the notification
	 * @return the x position of the notification
	 */
    public float getXpos()
    {
        return xpos;
    }

    /**
	 * Sets x position of the notification
	 * @param xpos x position of the notification
	 */
    public void setXpos(float xpos)
    {
        this.xpos = xpos;
    }

    
    /**
	 * Gets the y position of the notification
	 * @return the y position of the notification
	 */
    public float getYpos()
    {
        return ypos;
    }

    /**
	 * Sets the y position of the notification
	 * @param ypos the y position of the notification
	 */
    public void setYpos(float ypos)
    {
        this.ypos = ypos;
    }
    
    /**
	 * Draws the notification to the screen
	 * @param gr graphics object
	 */
    public void draw(Graphics gr)
    {
        Color oldColor = gr.getColor();
        
        // Draw the notification box
        gr.setColor(NOTIFICATION_FILL_COLOR);
        gr.fillRoundRect(this.xpos, this.ypos, gr.getFont().getWidth(this.message)+LEFT_RIGHT_BOX_MARGIN, gr.getFont().getHeight(this.message)+BOTTOM_UP_BOX_MARGIN, BOX_ROUND_RADIUS);
        gr.setColor(oldColor);
        
        // Draw the notification message
        gr.setColor(Color.black);
        gr.drawString(this.message, this.xpos + MESSAGE_OFFSET_X, this.ypos + MESSAGE_OFFSET_Y);
    }
}