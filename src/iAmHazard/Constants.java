package iAmHazard;

/**
 * Constant (abstract) class
 * A list of (configuration) Constants used throughout the game.
 * This class is not intended to have any instance.
 */
public abstract class Constants
{
	/** State ID for Login */
	public static final int STATE_ID_LOGIN = 0;
	/** State ID for Menu */
	public static final int STATE_ID_MENU = 1;
	/** State ID for Game play */
	public static final int STATE_ID_GAMEPLAY = 2;
	/** State ID for Performance Summary */
	public static final int STATE_ID_PERFORMANCE = 3;
    /** State ID for Login Screen */
    public static final int STATE_ID_TUTORIAL = 4;
	/** Number of entries in the high scores list */
	public static final int LIMIT_HIGHSCORES = 8;
	/** Path to the all users data files */
	public static final String USERS_DIR = "users/";
	/** Path to topics directory */
	public static final String TOPICS_DIR = "topics/";
	/** Path to image for monsters directory */
	public static final String IMAGES_MONSTERS_DIR = "images/monsters/";
    /** Path to image for app icon */
    public static final String ICON_IMAGE = "images/misc/icon.png";
	/** Game difficulty level Easy */
	public static final int EASY = 0;
	/** Game difficulty level Medium */
	public static final int MEDIUM = 1;
	/** Game difficulty level Hard */
	public static final int HARD = 2;
	/** Width of the screen */
	public static final int SCREENWIDTH = 800;
	/** Height of the screen */
	public static final int SCREENHEIGHT = 600;
	/** Contains the list of forbidden characters inside a user name */
	public static final String UNALLOWED_CHARACTERS = "\\/:*?\"<>|";
	/** List of available levels in the game */
	public static final Level[] LEVELS = {
		//         name			monstSpeed	monstCount	monstDistance	numOfRows	PlayerHealth	score	Powerups // Change the layout if not good
		new Level("EASY",		0.015f,		20,			500,			4,			5,				10,		new Powerup[]{new InstakillPowerup(3),new AddHealthPowerup(3),new TimeShiftPowerup(3)}),
        new Level("MEDIUM",		0.03f,		20,			500,			4,			4,				20,		new Powerup[]{new InstakillPowerup(2),new AddHealthPowerup(2),new TimeShiftPowerup(2)}),
        new Level("PRO",		0.04f,		20,			500,			4,			3,				30,		new Powerup[]{new InstakillPowerup(1),new AddHealthPowerup(1),new TimeShiftPowerup(1)}),
    };

    /** Variance in Monster placements */
    public static final int MAXVARIANCE = 50;
    public static final float VARIANCE = 0.5f;

    /** INDEX FOR POWERUPS */
    public static final int INSTAKILL_INDEX = 0;
    public static final int ADDHEALTH_INDEX = 1;
    public static final int TIMESHIFT_INDEX = 2;
}