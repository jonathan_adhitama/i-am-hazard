package iAmHazard;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.gui.TextField;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

/**
 * LoginState class
 * Displays an interface for player to login to the game.
 */
public class LoginState extends BasicGameState
{
	private int stateID = Constants.STATE_ID_LOGIN;
	
	/* General */
	private StateBasedGame sbg;
	
	/* Username text field */
	private TextField usernameTextField;
	private static final int USERNAME_TEXTFIELD_X = 150;
	private static final int USERNAME_TEXTFIELD_Y = 350;
	private static final int USERNAME_TEXTFIELD_WIDTH = 510;
	private static final int USERNAME_TEXTFIELD_HEIGHT = 35;
	
	/* Login background */
	private Image loginBackground;
	private static final String LOGIN_BACKGROUND_IMAGE = "images/state_login/login_bg.png";
	private static final float LOGIN_BACKGROUND_X = 0;
	private static final float LOGIN_BACKGROUND_Y = 0;
	private static final float LOGIN_BACKGROUND_SCALE = 0.65f;
	
	/* Login button */
	private Image loginButtonImage;
	private static final float LOGIN_BUTTON_X = 285;
	private static final float LOGIN_BUTTON_Y = 400;
	private static final String LOGIN_BUTTON_IMAGE = "images/state_login/login_button.png";
	private static final float LOGIN_BUTTON_SCALE = 0.63f;
	
	/* Error message */
	private String errorMessage;
	private boolean needToDrawErrorMessage;
	private static final int ERROR_MESSAGE_X = 300;
	private static final int ERROR_MESSAGE_Y = 550;
	
	/**
	 * Gets the ID of LoginState
	 * @return ID of LoginState
	 */
	@Override
	public int getID()
	{
		return this.stateID;
	}

	/**
	 * Initializes the game state
	 * @param gc GameContainer Object
	 * @param sbg StateBasedGame Object
	 * @throws SlickException
	 */
	@Override
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException
	{
		/* General */
		this.sbg = sbg;
		
		/* Username text field */
		this.usernameTextField = new TextField(gc, gc.getDefaultFont(), USERNAME_TEXTFIELD_X, USERNAME_TEXTFIELD_Y, USERNAME_TEXTFIELD_WIDTH, USERNAME_TEXTFIELD_HEIGHT);
		
		/* Login background */
		this.loginBackground = new Image(LOGIN_BACKGROUND_IMAGE);
		
		/* Login button */
		this.loginButtonImage = new Image(LOGIN_BUTTON_IMAGE);
    }
	
	/**
	 * Performs tasks when the game state is entered
	 * @param gc GameContainer Object
	 * @param sbg StateBasedGame Object
	 */
	@Override
	public void enter(GameContainer gc, StateBasedGame sbg)
	{
		/* Username text field */
		this.usernameTextField.setFocus(true);
		this.usernameTextField.setBackgroundColor(Color.transparent); 
		this.usernameTextField.setBorderColor(Color.transparent);
		this.usernameTextField.setTextColor(Color.black);
		
		/* Error message */
		this.needToDrawErrorMessage = false;
	}

	/**
	 * Renders the game state to the screen
	 * @param gc GameContainer Object
	 * @param sbg StateBasedGame Object
	 * @param gr Graphics Object
	 */
	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics gr)
	{
		/* Background */
		this.loginBackground.draw(LOGIN_BACKGROUND_X, LOGIN_BACKGROUND_Y, LOGIN_BACKGROUND_SCALE);
		this.usernameTextField.render(gc, gr);
		
		/* Login button */
		this.loginButtonImage.draw(LOGIN_BUTTON_X, LOGIN_BUTTON_Y, LOGIN_BUTTON_SCALE);
		
		/* Error message */
		if (this.needToDrawErrorMessage) {
			gr.setColor(Color.white);
			gr.drawString(this.errorMessage, ERROR_MESSAGE_X, ERROR_MESSAGE_Y);
		}
	}

	/**
	 * Updates the game state
	 * @param gc GameContainer Object
	 * @param sbg StateBasedGame Object
	 * @param delta time passed since last frame (milliseconds)
	 */
	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta)
	{	
		Input input = gc.getInput();

		if (input.isKeyPressed(Input.KEY_ENTER)) {
			doLogin(sbg);
		} else if (gc.getInput().isKeyPressed(Input.KEY_F5)) {
			sbg.enterState(Constants.STATE_ID_MENU);
		}
		
		if (!this.usernameTextField.hasFocus()) {
			this.usernameTextField.setFocus(true);
		}
	}

	/**
	 * Handles the event when a mouse button is released (un-clicked)
	 * @param button denotes which mouse button is released
	 * @param x the x coordinate where the click was made 
	 * @param y the y coordinate where the click was made
	 */
	@Override
	public void mouseReleased(int button, int x, int y)
	{
		if (x >= USERNAME_TEXTFIELD_X && x <= USERNAME_TEXTFIELD_X + USERNAME_TEXTFIELD_WIDTH &&
			y >= USERNAME_TEXTFIELD_Y && y <= USERNAME_TEXTFIELD_Y + USERNAME_TEXTFIELD_HEIGHT) {
			this.usernameTextField.setFocus(true);
		} else if (x >= LOGIN_BUTTON_X && x <= LOGIN_BUTTON_X + this.loginButtonImage.getWidth()*LOGIN_BUTTON_SCALE &&
			       y >= LOGIN_BUTTON_Y && y <= LOGIN_BUTTON_Y + this.loginButtonImage.getHeight()*LOGIN_BUTTON_SCALE) {
			// Process the username in text field
			doLogin(this.sbg);
		}
	}
	
	/* Checks whether the username the user entered is valid
	 * @return returns true if the string does not contain any of the characters in Constants.UNALLOWED_CHARACTERS
	 */
	private boolean validateUsername(String input)
	{
		boolean output = true;
		if (input.equals("")) { // username cannot be empty
			return false;
		}
		for (int i = 0; i < Constants.UNALLOWED_CHARACTERS.length(); i++) {
			if (input.contains(Character.toString(Constants.UNALLOWED_CHARACTERS.charAt(i)))) {
				output = false;
				break;
			}
		}
		return output;
	}
	
	/* This method tries to "login" a given username, and if successful, enters the state of menu
	 */
	private void doLogin(StateBasedGame sbg) {
		String username = this.usernameTextField.getText();
		if (validateUsername(username)) {
			User user = null;
			try {
				user = UserManager.login(username);
			} catch (Exception e) {
				e.printStackTrace();
				System.err.println("Failed to log the user in");
			}
			
			GameData.setUser(user);
			sbg.enterState(Constants.STATE_ID_MENU);
		} else {
			this.errorMessage = "Invalid username";
			this.needToDrawErrorMessage = true;
		}
	}
}