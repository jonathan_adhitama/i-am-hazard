package iAmHazard;

import java.util.ArrayList;

/**
 * TimeShiftPowerup class
 * Represents the powerup to slow the monsters down
 */
public class TimeShiftPowerup extends Powerup
{
	private static final String DESCRIPTION = "Slowing monsters down";
	private static final String IMAGE_LOCATION = "images/powerups/timeShift.png";
	private static final int DURATION = 5000;
	private static final float slowMultiplier = 0.3f;
	
	/**
	 * Constructor
	 * @param multiplicity powerup multiplicity
	 */
	public TimeShiftPowerup (int multiplicity)
	{
		super(DESCRIPTION, DURATION, IMAGE_LOCATION, multiplicity);
	}
	
	/**
	 * Slows the movement speed of monsters in the game
	 * @param gameState the gameplay state
	 * @return True if successfully slows the monsters
	 */
	@Override
	protected boolean doUpdate(GameplayState gameState)
	{
		float newSpeed = gameState.getLevel().getMonsterSpeed() * slowMultiplier;
		this.setMonsterSpeed(gameState, newSpeed);
		return true;
	}
	
	/**
	 * Restores the original monster speed
	 * @param gameState the gameplay state
	 */
	@Override
	public void deactivate(GameplayState gameState) 
	{
		this.setMonsterSpeed(gameState, gameState.getLevel().getMonsterSpeed());
	}
	
	/* Sets all monsters' speed to the given speed
	 * @param gameState GameplayState object
	 * @param speed The new speed to assign to every monster
	 */
	private void setMonsterSpeed(GameplayState gameState, float speed)
	{
		ArrayList<Monster> monsters = gameState.getMonsters();
		for (Monster monster : monsters) {
			monster.setSpeed(speed);
		}
	}
}