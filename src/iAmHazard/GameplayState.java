package iAmHazard;

import org.newdawn.slick.*;
import org.newdawn.slick.gui.TextField;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * GameplayState class
 * Displays the arena to play the game.
 */
public class GameplayState extends BasicGameState
{
    /* Level Settings */
    private Level level;

    /* Image general */
    private static final float imgScale = 0.64f;
    private static final float bgScale = 0.64f;
    private static final float monsterScale = 0.4f;

    /* Player */
    private Player player;
    private static final float PLAYER_INIT_X = 50;
    private static final float PLAYER_INIT_Y = 220;
    private static final String PLAYER_IMAGE = "images/player/player1.png";

    /* Monsters */
    private ArrayList<Monster> monsters;
    private Hashtable<Float, Monster> frontmostMonsterTable;

    /* Arena */
    private static final float ARENA_TOP_MARGIN = 85;
    private static final float ARENA_BOTTOM_MARGIN = 530;

    private static final float PLAYER_REGION_THRESHOLD = 100;

    private Image arenaBars;
    private static final String ARENA_BARS_IMAGE = "images/state_gameplay/arena_bars.png";
    private static final float ARENA_BAR_X = 0;
    private static final float ARENA_BAR_Y = 0;
    private Image arenaBG;
    private static final String ARENA_BG_IMAGE_PRE = "images/state_gameplay/arena_bg ";
    private static final String ARENA_BG_IMAGE_SUF = ".png";
    private static final float ARENA_BG_X = 0;
    private static final float ARENA_BG_Y = 0;

    private Image iconHealth;
    private static final String ICON_HEALTH_IMAGE = "images/state_gameplay/icon_health.png";
    private static final float ICON_HEALTH_IMAGE_X = 27;
    private static final float ICON_HEALTH_IMAGE_Y = 28;
    private static final float ICON_HEALTH_IMAGE_DIFF = -7;

    private static final float POWERUP_X = 300;
    private static final float POWERUP_Y = 19;
    private static final float POWERUP_IMAGE_DIFF = 45;
    private static final float POWERUP_MULT_OFFSET_X = 17;
    private static final float POWERUP_MULT_OFFSET_Y = 41;
    private static final int[] POWERUP_KEYS = {Input.KEY_F1,Input.KEY_F2,Input.KEY_F3,Input.KEY_F4,Input.KEY_F5,Input.KEY_F6,Input.KEY_F7,Input.KEY_F8,Input.KEY_F9,Input.KEY_F10,Input.KEY_F11,Input.KEY_F12,Input.KEY_F13,Input.KEY_F14,Input.KEY_F15};
    
    private static final int SCORE_X = 680;
    private static final int SCORE_Y = 40;

    private TextField answerTextField;
    private static final int ANSWER_TEXTFIELD_X = 45;
    private static final int ANSWER_TEXTFIELD_Y = 550;
    private static final int ANSWER_TEXTFIELD_WIDTH = 630;
    private static final int ANSWER_TEXTFIELD_HEIGHT = 32;

    /* Notifications */
    private Notifier notifier;
    private static final float NOTIFICATION_X = 65;
    private static final float NOTIFICATION_Y = 500;
    private static final float NOTIFICATION_UPDATE_X_DIFF = 0;
    private static final float NOTIFICATION_UPDATE_Y_DIFF = -0.03f;
    private static final int NOTIFICATION_TIME = 3000;
    
    /* Motion */
    private Monster monsterToKill;
    
    /**
	 * Gets the Player object in the game session
	 * @return Player object
	 */
	public Player getPlayer()
	{
		return this.player;
	}
	
	/**
	 * Gets the Level object which contains the game settings
	 * used in the game session
	 * @return Level object for the game session
	 */
	public Level getLevel()
	{
		return this.level;
	}
    
    /**
     * Gets the list of Monster objects used in the game session
     * @return list of Monster objects
     */
    public ArrayList<Monster> getMonsters()
    {
    	return this.monsters;
    }
	
	/**
	 * Gets the ID of MenuState
	 * @return ID of MenuState
	 */
	@Override
    public int getID()
    {
        return Constants.STATE_ID_GAMEPLAY;
    }

	/**
	 * Initializes the game state
	 * @param gc GameContainer Object
	 * @param sbg StateBasedGame Object
	 * @throws SlickException
	 */
	@Override
    public void init(GameContainer gc, StateBasedGame sbg)
    {
		
    }
	
	/**
	 * Performs tasks when the game state is entered
	 * @param gc GameContainer Object
	 * @param sbg StateBasedGame Object
	 */
    @Override
    public void enter(GameContainer gc, StateBasedGame sbg)
    {
        try {
            /* Level Settings */
            this.level = Constants.LEVELS[GameData.getLevel()];

            /* Player */
            this.player = new Player(PLAYER_INIT_X, PLAYER_INIT_Y, new Image(PLAYER_IMAGE), this.level.getHealth());

            /* Monsters */
            this.monsters = MonsterFactory.getMonsters(QuestionManager.parseQuestions(GameData.getTopic()), GameData.getLevel(), ARENA_TOP_MARGIN, ARENA_BOTTOM_MARGIN);
            for (Monster monster : monsters) {
            	monster.move(0,monster.getYposition()); // Move monsters to the left
            }
            this.frontmostMonsterTable = new Hashtable<Float, Monster>();
            
            /* Monster killing */
            this.monsterToKill = null;
            /* Badges */
            GameData.resetNewBadges();

            /* Score */
            GameData.setScore(0);

            /* Question set */
            GameData.resetQuestionSet();

            /* Images */
            this.arenaBars = new Image(ARENA_BARS_IMAGE);
            this.arenaBG = new Image(ARENA_BG_IMAGE_PRE + GameData.getTopic() + ARENA_BG_IMAGE_SUF);
            this.iconHealth = new Image(ICON_HEALTH_IMAGE);

            /* Answer Text Field */
            this.answerTextField = new TextField(gc, gc.getGraphics().getFont(), ANSWER_TEXTFIELD_X, ANSWER_TEXTFIELD_Y, ANSWER_TEXTFIELD_WIDTH, ANSWER_TEXTFIELD_HEIGHT);
            this.answerTextField.setBackgroundColor(Color.transparent);
            this.answerTextField.setBorderColor(Color.transparent);
            this.answerTextField.setTextColor(Color.black);

            /* Notifier */
            this.notifier = new Notifier(NOTIFICATION_X, NOTIFICATION_Y, NOTIFICATION_UPDATE_X_DIFF, NOTIFICATION_UPDATE_Y_DIFF, NOTIFICATION_TIME);
            this.notifier.notify("Let's start!");

            
            /* Powerups */
            for (Powerup powerup : this.level.getPowerups()) {
            	powerup.init();
            	this.player.addPowerup(powerup);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Gameplay initializing error.");
            System.exit(1);
        }
    }

    /**
	 * Renders the game state to the screen
	 * @param gc GameContainer Object
	 * @param sbg StateBasedGame Object
	 * @param gr Graphics Object
	 */
	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics gr)
    {
        /* General color */
        gr.setColor(Color.white);
        
        /* Arena */
        this.arenaBG.draw(ARENA_BG_X, ARENA_BG_Y, bgScale);
        this.arenaBars.draw(ARENA_BAR_X, ARENA_BAR_Y, bgScale);

        /* Top bar */     
        for (int i=0; i<this.player.getHealth(); i++) {
            float healthImageOffset = i * (this.iconHealth.getWidth()*imgScale + ICON_HEALTH_IMAGE_DIFF);
            this.iconHealth.draw(ICON_HEALTH_IMAGE_X+healthImageOffset,ICON_HEALTH_IMAGE_Y,imgScale);
        }
        
        float powerupOffset = 0;
        ArrayList<Powerup> powerups = this.player.getPowerups();
        for (int i=0; i<powerups.size(); i++) {
        	Powerup powerup = powerups.get(i);
        	powerup.getImage().draw(POWERUP_X+powerupOffset, POWERUP_Y, imgScale);
            gr.drawString(Integer.toString(powerup.getMultiplicity()),
                            POWERUP_X+powerupOffset+POWERUP_MULT_OFFSET_X,
                            POWERUP_Y+POWERUP_MULT_OFFSET_Y);
            powerupOffset += POWERUP_IMAGE_DIFF;
        }
        
        gr.drawString(Integer.toString(GameData.getScore()), SCORE_X, SCORE_Y);

        /* Bottom bar */
        this.answerTextField.render(gc, gr);

        /* Player */
        this.player.draw(gr, imgScale);

        /* Monster */
        for (Monster monster : this.monsters) {
            monster.draw(gr, monsterScale);
        }
        Enumeration<Monster> frontmostMonsters = this.frontmostMonsterTable.elements();
        while (frontmostMonsters.hasMoreElements()) {
            frontmostMonsters.nextElement().drawQuestion(gr);
        }

        this.notifier.render(gr);
	}

	/**
	 * Updates the game state
	 * @param gc GameContainer Object
	 * @param sbg StateBasedGame Object
	 * @param delta time passed since last frame (milliseconds)
	 */
	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta)
    {
		/* Update references to frontmost monsters */
        this.updateTable();

        Input input = gc.getInput();

        /* Answer */
        if (input.isKeyPressed(Input.KEY_ENTER)) {
            doAnswer(this.answerTextField.getText());
        }
        
        /* Powerup */
        for (int i=0; i<this.player.getPowerups().size(); i++) {
        	if (input.isKeyPressed(POWERUP_KEYS[i])) {
	        	Powerup powerup = this.player.getPowerups().get(i);
	        	if (powerup.activate(this)) {
	        		this.notifier.notify(powerup.getDescription());
	        	} else {
	        		this.notifier.notify("Cannot activate "+powerup.getDescription());
	        	}
        	}
        }
        
        for (Powerup powerup : this.player.getPowerups()) {
        	powerup.update(this, delta);
        }
        
        /* Monster(s) too close to player */
        attackPlayer();

        /* End game if over */
        boolean killedAllMonster = true;
        for (Monster monster : this.monsters) {
        	if (monster.isExistent()) {
        		killedAllMonster = false;
        		break;
        	}
        }
        
        if (player.getHealth() <= 0 || killedAllMonster) {
            endGame(sbg);
        }

        /* Make sure the answer textfield is always in focus */
        if (!this.answerTextField.hasFocus()) {
            this.answerTextField.setFocus(true);
        }
        
        /* Update Player and Monster's positions */
        this.player.updatePosition(delta);
        for (Monster monster : this.monsters) {
            if (monster.isExistent()) {
                monster.updatePosition(delta);
            }
        }

        /* Monster killing update */
        killMonster(delta);
        
        /* Notifier update */
        this.notifier.update(delta);
	}

	/* Checks if the answer matches with any question and 
	 * @param answerTyped the answer to be checked
	 * @return a Monster object that carries the answered question. If no
	 * question is answered, this method will return NULL.
	 */
    private boolean doAnswer(String answerTyped)
    {
        Monster whichAnswered = null;
        Enumeration<Monster> frontmostMonsters = this.frontmostMonsterTable.elements();
        while (frontmostMonsters.hasMoreElements()) {
            Monster aMonster = frontmostMonsters.nextElement();
            if ((aMonster.isExistent()) && (aMonster.getQuestion().getAnswer().equalsIgnoreCase(answerTyped))) {
                whichAnswered = aMonster;
                break;
            }
        }
        
        if (whichAnswered != null) {
        	GameData.setScore(GameData.getScore()+whichAnswered.getScore());
	        this.monsterToKill = whichAnswered;
	        whichAnswered.setSpeed(0);
	        this.player.move(this.player.getXposition(), whichAnswered.getYposition());
	        this.answerTextField.setText("");
	        this.notifier.notify("Correct! +" + whichAnswered.getScore());
	        return true;
        } else {
        	this.notifier.notify("Incorrect.");
        	return false;
        }
    }
    
    /* Determines whether to it's time to kill the Monster object set in
     * monsterToKill variable. If it is time, the Monster object will be
     * killed.
     * @param delta time passed since last frame (milliseconds)
     */
    private void killMonster(int delta)
    {
    	if (this.player.isMoving()) {
    		return;
    	}
    	
    	if (this.monsterToKill != null) {
        	this.monsterToKill.kill(true);
        	this.monsterToKill = null;
    	}
    }

    /* Updates the frontmostMonsterTable variable to make sure it keeps the
     * correct Monster objects, i.e. the frontmost monsters for every row.
     */
    private void updateTable()
    {
        for (Monster aMonster : this.monsters) {
            if (aMonster.isExistent()) {
                if (!(this.frontmostMonsterTable.containsKey(aMonster.getYposition()))) {
                    // Key does not exists in hash frontmostMonsterTable
                    this.frontmostMonsterTable.put(aMonster.getYposition(), aMonster);
                } else {
                    // Key already exists in hash frontmostMonsterTable
                    // Need check whether which monster is closer to player
                    if (!this.frontmostMonsterTable.get(aMonster.getYposition()).isExistent() ||
                        this.frontmostMonsterTable.get(aMonster.getYposition()).getXposition() > aMonster.getXposition()) {
                        this.frontmostMonsterTable.put(aMonster.getYposition(), aMonster);
                    }
                }
            }
        }
    }

    /* Performs the necessary actions when the Player is hit by a Monster.
     */
    private void attackPlayer()
    {
        for (Monster aMonster : this.monsters) {
            if (aMonster.isExistent() && aMonster.getXposition() < PLAYER_REGION_THRESHOLD) {
            	aMonster.kill(false);
                player.setHealth(player.getHealth() - 1);
            }
        }
    }

    /* Performs the necessary actions when the game has ended
     * @param sbg StateBasedGame Object
     */
    private void endGame(StateBasedGame sbg)
    {
    	// Send question set to GameData
        for (Monster monster : this.monsters) {
            GameData.addNewQuestion(monster.getQuestion());
        }
    	
        // Update user score and badges
        GameData.getUser().updateHighscore(GameData.getScore());
        for (Badge badge : BadgeManager.getAllQualifiedBadges(this)) {
            GameData.addNewBadge(badge);
            GameData.getUser().addBadge(badge);
        }

        // Save user
        try {
            UserManager.saveUser(GameData.getUser());
        } catch (IOException e) {
            System.err.println("Error: unable to save user data. " + e.toString());
        }

        // Change state
        sbg.enterState(Constants.STATE_ID_PERFORMANCE);
    }
}