package iAmHazard;

import java.util.ArrayList;
import java.util.Iterator;

import org.newdawn.slick.Image;

/**
 * Player class
 * Represents a Player object inside the GameplayState
 */
public class Player extends GameObject
{
	private int health;
	private ArrayList<Powerup> powerups;
	private static final float SPEED = 1.0f;

	/**
	 * Constructor
	 * @param xposition the x-position of the Player relative to the screen
	 * @param yposition the y-position of the Player relative to the screen
	 * @param image the Image object to draw the Player
	 * @param health initial health of the Player
	 */
	public Player(float xposition, float yposition, Image image, int health)
	{
		super(xposition,yposition,image,SPEED);
		this.health = health;
		this.powerups = new ArrayList<Powerup>();
	}

	/**
	 * Gets the health of the player
	 * @return health of the player
	 */
	public int getHealth()
	{
		return this.health;
	}

	/**
	 * Gets the list of powerups of the player
	 * @return list of powerups of the player
	 */
    public ArrayList<Powerup> getPowerups()
    {
        return this.powerups;
    }

    /**
	 * Sets the health of player
	 * @param health the health of the player
	 */
	public void setHealth(int health)
	{
		this.health = health;
	}

	/**
	 * Adds/attaches a Powerup object to the Player object.
	 * @param powerup the Powerup object to be added to the Player
	 */
	public void addPowerup(Powerup powerup)
	{
		// Checks if the Player already has the powerup
		String newPowerupDesc = powerup.getDescription();
		Iterator<Powerup> powerupIter = this.powerups.iterator();
		while (powerupIter.hasNext()) {
			Powerup aPowerup = powerupIter.next();
			if (newPowerupDesc.equals(aPowerup.getDescription())) {
				// Increment the multiplicity of the Badge
				aPowerup.setMultiplicity(aPowerup.getMultiplicity()+powerup.getMultiplicity());
				return;
			}
		}
		
		// The Player has not have the Powerup; put the new Powerup to the Player
		this.powerups.add(powerup);
	}

	/**
	 * Activates Player's powerup at the specified index
	 * @param index the index in the powerups list
	 * @param gps GameplayState
	 * @return true if the powerup is put to effect
	 */
	public boolean usePowerup(int index, GameplayState gps)
	{
		return this.powerups.get(index).activate(gps);
	}
}