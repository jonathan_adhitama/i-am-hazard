package iAmHazard;

import java.util.ArrayList;

/**
 * GameData class
 * Keeps record of the game data that needs to be communicated
 * across game states. This class consists of static methods, accessible
 * from anywhere in the program.
 */
public class GameData
{
	private static int level;
	private static int score;
	private static String topic;
	private static ArrayList<Question> questionSet = new ArrayList<Question>();
	private static ArrayList<Badge> newBadges = new ArrayList<Badge>();
	private static User user;
	
	/**
	 * Gets the level of the game session
	 * @return level of the game session
	 */
	public static int getLevel()
	{
		return level;
	}
	
	/**
	 * Sets the level of the game session
	 * @param level the level of the game session
	 */
	public static void setLevel(int level)
	{
		GameData.level = level;
	}

	/**
	 * Gets the score achieved by the user in the game session
	 * @return the score of the user in the game session
	 */
	public static int getScore()
	{
		return score;
	}
	
	/**
	 * Sets the score of the player in the game session
	 * @param score the score of the player in the game session
	 */
	public static void setScore(int score)
	{
		GameData.score = score;
	}
	
	/**
	 * Gets the topic of the question to be used in the game session
	 * @return the topic of the question to be used in the game session
	 */
	public static String getTopic()
	{
		return topic;
	}
	
	/**
	 * Sets the topic of the question to be used in the game session
	 * @param topic the selected topic to be used in the game session
	 */
	public static void setTopic(String topic)
	{
		GameData.topic = topic;
	}
	
	/**
	 * Gets the list of questions used in the game session
	 * @return the list of questions used in the game session
	 */
	public static ArrayList<Question> getQuestions()
	{
		return questionSet;
	}
	
	/**
	 * Adds a question into the list of questions used in the game session
	 * @param question question to be added into the list
	 */
	public static void addNewQuestion(Question question)
	{
		questionSet.add(question);
	}
	
	/**
	 * Delete all of the questions in the list of question
	 */
	public static void resetQuestionSet()
	{
		questionSet.clear();
	}
	
	/**
	 * Gets the list of badges in the game just achieved in the game session
	 * @return list of badges in the game just achieved in the game session
	 */
	public static ArrayList<Badge> getNewBadges()
	{
		return newBadges;
	}
	
	/**
	 * Adds a badge into the list of badges the player just achieved in the
	 * game session.
	 * @param badge the badge to be added into the list
	 */
	public static void addNewBadge(Badge badge)
	{
		newBadges.add(badge);
	}
	
	/**
	 * Deletes all of the badges in the list of badges
	 */
	public static void resetNewBadges()
	{
		newBadges.clear();
	}
	
	/**
	 * Gets the User object that is used to log in to the game
	 * @return the User object of the currently logged in user
	 */
	public static User getUser()
	{
		return user;
	}
	
	/**
	 * Sets the User object that is logged in to the game
	 * @param user User object that is logged in to the game
	 */
	public static void setUser(User user)
	{
		GameData.user = user;
	}
}