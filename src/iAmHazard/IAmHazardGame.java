package iAmHazard;

import org.newdawn.slick.*;
import org.newdawn.slick.state.StateBasedGame;

/**
 * IAmHazardGame class
 * Contains the driver of the I Am Hazard game
 */
public class IAmHazardGame extends StateBasedGame
{
	/**
	 * Constructor
	 * @throws Exception Indicates the game fails to load user data
	 */
	public IAmHazardGame() throws Exception
	{
		super("I am Hazard");
	
		UserManager.initialize();
		this.addState(new LoginState());
		this.addState(new MenuState());
        this.addState(new GameplayState());
        this.addState(new PerformanceSummaryState());
        this.addState(new TutorialState());
		
		this.enterState(Constants.STATE_ID_LOGIN);
	}

    /**
     * Starts the game
     * @param args Command line arguments
     */
    public static void main(String[] args) throws Exception
	{
		AppGameContainer app = new AppGameContainer(new IAmHazardGame());

		app.setDisplayMode(800, 600, false);
        app.setShowFPS(false);
        app.setIcon(Constants.ICON_IMAGE);
		app.start();
	}


	@Override
	public void initStatesList(GameContainer gc) throws SlickException
	{
		
	}
}