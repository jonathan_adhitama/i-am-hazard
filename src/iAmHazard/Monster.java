package iAmHazard;

import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

/**
 * Monster class
 * Represents a Monster object inside the GameplayState
 */
public class Monster extends GameObject
{
	private int score;
	private Question question;

	/**
	 * Constructor
	 * @param xposition The x-position of the Monster relative to the screen
	 * @param yposition The y-position of the Monster relative to the screen
	 * @param image The Image object to draw the Monster
	 * @param score How much point to be awarded when the Monster is killed
	 * @param question Question object that the Monster carries
	 */
	public Monster(float xposition, float yposition, Image image, int score, float speed, Question question)
	{
		super(xposition, yposition, image, speed);
		this.score = score;
		this.question = question;
	}
	
	/**
	 * Gets the Question that the monster carries
	 * @return Question of the monster
	 */
	public Question getQuestion()
	{
		return this.question;
	}

	/**
	 * Gets the score awarded to player when this monster is killed
	 * @return score of the monster
	 */
	public int getScore()
	{
		return this.score;
	}

	/**
	 * Draws the monster to the screen
	 * @param gr graphics object
	 * @param imgScale scale of the image 
	 */
	@Override
    public void draw(Graphics gr, float imgScale)
    {
        super.draw(gr, imgScale);
    }

    /**
	 * Draws the question that the monster carries to the screen
	 * @param gr graphics object
	 */
    public void drawQuestion(Graphics gr)
    {
        if (!this.isExistent()) {
            return;
        }
        
        Font font = gr.getFont();
        Color oldColor = gr.getColor();
        Color fillColor = new Color(15,15,15,217);
        int bubbleWidth = 300;
        int bubbleMargin = 10;
        String questionToDraw = this.getQuestion().wrappedQuestion(font, bubbleWidth-bubbleMargin*2);
        
        // Draw the question box
        gr.setColor(fillColor);
        gr.fillRoundRect((int)(this.getXposition()+40),
                         this.getYposition()-30, bubbleWidth, font.getHeight(questionToDraw)+2*bubbleMargin, 7);
        
        // Draw the question text, with a shadow
        font.drawString((int)(this.getXposition()+40+bubbleMargin+1),
        		        this.getYposition()-30+bubbleMargin+1, questionToDraw, Color.black);
        font.drawString((int)(this.getXposition()+40+bubbleMargin),
                        this.getYposition()-30+bubbleMargin, questionToDraw, Color.white);
        
        gr.setColor(oldColor);
    }
    
    /**
     * Kills a Monster object out of the game. This, however, does not mean the player
     * has answered the question correctly.
     * @param answeredCorrectly Denotes whether the question was answered correctly or not
     */
    public void kill(boolean answeredCorrectly)
    {
    	this.setExistent(false);
    	if (answeredCorrectly) {
    		this.question.markAnswered();
    	}
    }
}