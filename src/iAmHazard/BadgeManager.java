package iAmHazard;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * BadgeManager class
 * Consists of static methods to manage Badge objects in the game
 */
public class BadgeManager
{
	/**
	 * Copies a given Badge object
	 * @return A new Badge object
	 */
	public static Badge copy(Badge badge)
	{
		if (badge instanceof UntouchableBadge) {
			return new UntouchableBadge(badge.getMultiplicity());
		} else if (badge instanceof DeterminatorBadge) {
			return new DeterminatorBadge(badge.getMultiplicity());
		} else if (badge instanceof LikeABossBadge){
			return new LikeABossBadge(badge.getMultiplicity());
		} else {
			return null;
		}
	}
	
	/**
	 * Returns a list of all badges that the Player is qualified for.
	 * @param gps GameplayState object
	 * @return A list of qualified badges.
	 */
	public static ArrayList<Badge> getAllQualifiedBadges(GameplayState gps)
	{
		ArrayList<Badge> qualifiedBadges = new ArrayList<Badge>();
		
		// Iterate through all badges
		ArrayList<Badge> allBadges = getAllBadges();
		Iterator<Badge> badgeIter = allBadges.iterator();
		while (badgeIter.hasNext()) {
			Badge aBadge = badgeIter.next();
			if (aBadge.checkQualified(gps)) {
				qualifiedBadges.add(aBadge);
			}
		}
		
		return qualifiedBadges;
	}
	
	/* Gets all of the badges which exist in the game
	 * @return A list of all Badges
	 */
	private static ArrayList<Badge> getAllBadges()
	{
		ArrayList<Badge> allBadges = new ArrayList<Badge>();
		
		// List all subclasses of Badge class
		allBadges.add(new UntouchableBadge());
		allBadges.add(new DeterminatorBadge());
		allBadges.add(new LikeABossBadge());
		return allBadges;
	}
}