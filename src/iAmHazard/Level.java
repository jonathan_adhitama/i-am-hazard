package iAmHazard;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Level class
 * Represents a level in the game. An object of this class stores the
 * parameters to be used in the game session.
 */
public class Level
{
	/* General */
	private String name;
	
	/* Monster */
	private float monsterSpeed;
	private int monsterCount;
	private int numberOfRows;
	private int monsterDistance;
	private int score;
	
	/* Player */
	private int health;
	private ArrayList<Powerup> powerups;
	
	/**
	 * Constructor
	 * @param name name of the level
	 * @param monsterSpeed movement speed of the monster
	 * @param monsterCount total number of monsters in the game
	 * @param monsterDistance minimum distance between each monster in-game
	 * @param numberOfRows number of rows in the arena
	 * @param health number of health the player have
	 * @param score the score awarded to the player for a monster killed
	 * @param powerups the list of powerups the player can utilize in the game
	 */
	public Level(String name, float monsterSpeed, int monsterCount, int monsterDistance,
			int numberOfRows, int health, int score, Powerup[] powerups) {
		this.name = name;
		this.monsterSpeed = monsterSpeed;
		this.monsterCount = monsterCount;
		this.monsterDistance = monsterDistance;
		this.numberOfRows = numberOfRows;
		this.health = health;
		this.score = score;
		
		if (powerups == null) {
			this.powerups = new ArrayList<Powerup>();
		} else {
			this.powerups = new ArrayList<Powerup>(Arrays.asList(powerups));
		}
	}
	
	/**
	 * Gets the name of the level
	 * @return name of the level
	 */
	public String getName()
	{
		return name;
	}
	
	/**
	 * Gets the monster's movement speed in-game
	 * @return the monster's movement speed
	 */
	public float getMonsterSpeed()
	{
		return monsterSpeed;
	}
	
	/**
	 * Gets the total number of monsters in the game
	 * @return the number of monsters in the game
	 */
	public int getMonsterCount()
	{
		return monsterCount;
	}
	
	/**
	 * Gets the monster's minimum distance from one to another
	 * @return the monster's minimum distance from one to another
	 */
	public int getMonsterDistance()
	{
		return monsterDistance;
	}
	
	/**
	 * Gets the number of rows in the game
	 * @return the number of rows in the game
	 */
	public int getNumberOfRows()
	{
		return numberOfRows;
	}
	
	/**
	 * Gets the list of powerups that players can use in-game 
	 * @return the list of powerups
	 */
	public ArrayList<Powerup> getPowerups()
	{
		return powerups;
	}
	
	/**
	 * Gets the number of health the player has
	 * @return the number of health the player has
	 */
	public int getHealth()
	{
		return health;
	}
	
	/**
	 * Gets the score awarded to the player for each monster killed
	 * @return score the score
	 */
	public int getScore() 
	{
		return this.score;
	}
}