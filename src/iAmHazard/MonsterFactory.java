package iAmHazard;
import java.io.File;
import java.util.ArrayList;

import org.newdawn.slick.Image;

/**
 * MonsterFactory class
 * Consists of static methods that is responsible to manage monsters used in a
 * game session.
 */
public class MonsterFactory
{
	private static final float XPOS_BUFFER = 50;
	
	/**
	 * Creates the list of monsters in a single gameplay session
	 * @param questions the list of questions
	 * @param level the level of the gameplay
	 * @throws Exception 
	 * @return the list of monsters 
	 */
	public static ArrayList<Monster> getMonsters(ArrayList<Question> questions, int level, float topMargin, float bottomMargin) throws Exception
	{
		Level levelSettings = Constants.LEVELS[level];
		
		ArrayList<Image> images = getMonsterImages();
		
		ArrayList<Monster> monsters = new ArrayList<Monster>();

		// Some useful constants
		float screenWidth = Constants.SCREENWIDTH;
		int numOfRows = levelSettings.getNumberOfRows();
		float arenaHeight = bottomMargin - topMargin;
		float rowHeight = arenaHeight / numOfRows;
		int monsterDistance = levelSettings.getMonsterDistance();
		int monsterCount = levelSettings.getMonsterCount();
		int monsterPerRow = monsterCount / numOfRows; // Last row may be different
		int score = levelSettings.getScore();
		float monsterSpeed = levelSettings.getMonsterSpeed();
		

		// Building the Monster objects row by row
		for (int i=0; i<numOfRows; i++) {
			float ypos = i*rowHeight + rowHeight/2 + topMargin;
			float startXpos = screenWidth + XPOS_BUFFER;
            if (i != 0) {
                startXpos += randomizeIndex(monsterDistance);
            }
			
			// Start building Monster objects for the row
			int monstersInRow = (i<numOfRows-1)?monsterPerRow:(monsterPerRow+(monsterCount % numOfRows));
			for (int j=0; j<monstersInRow; j++) {
                float variance = randomizeIndex((int)(monsterDistance*Constants.VARIANCE));
				float xpos = startXpos + j*monsterDistance + variance;
				int randomImageIndex = randomizeIndex(images.size());
				Image image = images.get(randomImageIndex);
				Question question = takeOneQuestion(questions);
				if (question == null) {
					throw new Exception ("Insufficient number of questions for selected topic and level. At least " + monsterCount + " questions are needed.");
				}
				
				Monster aMonster = new Monster(xpos,ypos,image,score,monsterSpeed,question);
				monsters.add(aMonster);
			}
		}
		return monsters;
	}
	
	/* From a list of questions, take a random question
	 * @param questions the question set to take the question from
	 * @return the randomly selected question. Returns null if the question set
	 * is empty
	 */
	private static Question takeOneQuestion(ArrayList<Question> questions)
	{
		if (questions.isEmpty()) {
			return null;
		}
		int index = randomizeIndex(questions.size());
		Question q = questions.remove(index);
		return q;
	}
	
	/* Gets a list of images from a given directory
	 * @return list of monster images
	 * @throws Exception Indicates the monster images cannot be found.
	 */
	private static ArrayList<Image> getMonsterImages() throws Exception
	{
		ArrayList<Image> images = new ArrayList<Image>();
		
		File monsterImagesFolder = new File(Constants.IMAGES_MONSTERS_DIR);	
		File[] monsterImagesFileList = monsterImagesFolder.listFiles();
		if (monsterImagesFileList == null) {
			throw new Exception ("Monster images not found.");
		}
		
		for (File monsterImageFile : monsterImagesFileList) {
			if (monsterImageFile.getName().endsWith("png")) {
				try {
					images.add(new Image(Constants.IMAGES_MONSTERS_DIR + monsterImageFile.getName()));
				} catch (Exception e) {
					System.err.println("Can't make monster image from " + monsterImageFile.getName() + ". " + e.toString());
				}
			}
		}
		
		return images;
	}
	
	/* For a given length of a list, select a random index
	 * @return randomly selected index
	 */
	private static int randomizeIndex(int length)
	{
		return (int) (Math.random()*length);
	}
}