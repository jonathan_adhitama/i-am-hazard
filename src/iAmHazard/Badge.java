package iAmHazard;

import java.io.Serializable;

/**
 * Badge (abstract) class
 * Represents a badge used in the game
 */
public abstract class Badge implements Serializable
{
	private static final long serialVersionUID = 8959461157737991427L;
	private String name;
	private int multiplicity;
	
	/**
	 * Constructor
	 * @param name badge name
	 * @param multiplicity badge multiplicity
	 */
	public Badge(String name, int multiplicity)
	{
		this.name = name;
		this.multiplicity = multiplicity;
	}
	
	/**
	 * Constructor
	 * @param name Badge name
	 */
	public Badge(String name)
	{
		this(name, 1);
	}
	
	/**
	 * Gets the name of the badge
	 * @return the name of the badge
	 */
	public String getName()
	{
		return this.name;
	}
	
	/**
	 * Gets the multiplicity of the badge
	 * @return the multiplicity of the badge
	 */
	public int getMultiplicity()
	{
		return this.multiplicity;
	}
	
	/**
	 * Sets the multiplicity of the badge
	 * @param multiplicity the new multiplicity of the badge  
	 */
	public void setMultiplicity(int multiplicity)
	{
		this.multiplicity = multiplicity;
	}
	
	/**
	 * Checks if the Player is eligible to get this Badge object.
	 * @param gps GameplayState object
	 * @return True if the Player is eligible, False otherwise.
	 */
	public abstract boolean checkQualified(GameplayState gps);
}