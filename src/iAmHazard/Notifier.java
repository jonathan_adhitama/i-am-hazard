package iAmHazard;

import org.newdawn.slick.Graphics;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Notifier class
 * Acts as a notifications manager. All notifications are meant to be managed
 * through the interfaces of this class.
 */
public class Notifier
{
    private ArrayList<Notification> notifications;
    private float notification_x;
    private float notification_y;
    private float update_x_diff;
    private float update_y_diff;
    private int notification_time;

    /**
	 * Constructor
	 * @param notification_x starting x-position of notification on screen
	 * @param notification_y starting y-position of notification on screen
	 * @param update_x_diff change in x-position per ms
	 * @param update_y_diff change in y-position per ms
	 * @param notification_time total time each notification is displayed on screen in ms
	 */
    public Notifier(float notification_x, float notification_y, float update_x_diff, float update_y_diff, int notification_time)
    {
        this.notifications = new ArrayList<Notification>();
        this.notification_x = notification_x;
        this.notification_y = notification_y;
        this.update_x_diff = update_x_diff;
        this.update_y_diff = update_y_diff;
        this.notification_time = notification_time;
    }

    /**
	 * Creates a new notification to the player.
	 * @param message notification message to display on screen
	 */
    public void notify(String message)
    {
        this.notifications.add(new Notification(message, this.notification_time, this.notification_x, this.notification_y));
    }

    /**
	 * Updates the positions of the notifications to be displayed to the
	 * player. This method also removes expired notifications.
	 * @param delta time passed since last frame (milliseconds).
	 */
    public void update(int delta)
    {
        Iterator<Notification> notifs = this.notifications.iterator();
        while (notifs.hasNext()) {
            Notification notif = notifs.next();
            if (notif.getTimeRemaining() <= 0) {
                notifs.remove();
                continue;
            } else {
                notif.setTimeRemaining(notif.getTimeRemaining() - delta);
                notif.setXpos(notif.getXpos() + this.update_x_diff*delta);
                notif.setYpos(notif.getYpos() + this.update_y_diff*delta);
            }
        }
    }

    /**
	 * Renders the notifications on the screen.
	 * @param gr graphics object
	 */
    public void render(Graphics gr)
    {
        for (Notification notif : this.notifications) {
            notif.draw(gr);
        }
    }
}