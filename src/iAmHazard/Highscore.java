package iAmHazard;

/**
 * Highscore class
 * Represents a highscore entry in the game.
 */
public class Highscore 
{
	private String username;
	private int score;

	/**
	 * Constructor
	 * @param username the name of the user
	 * @param score the user's score
	 */
	public Highscore(String username, int score)
	{
		this.username = username;
		this.score = score;
	}

	/**
	 * Gets the name of the user
	 * @return name of the user
	 */
	public String getUsername()
	{
		return this.username;
	}
	
	/**
	 * Gets the score of the user
	 * @return the score of the user
	 */
	public int getScore()
	{
		return this.score;
	}
}