package iAmHazard;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

/**
 * Powerup (abstract) class
 * Represents a Powerup object in the game.
 */
public abstract class Powerup
{
	private boolean activated;
	private String description;
	private int duration;
	private String imageLocation;
	private Image image;
	private int multiplicity;
	private int defaultMultiplicity;
	private int timeRemaining;

	/**
	 * Constructor
	 * @param description the description of the powerup
	 * @param duration the duration of the powerup
	 * @param imageLocation the String that describes the location of the image file of the powerup
	 * @param multiplicity the multiplicity of the powerup
	 */
	public Powerup(String description, int duration, String imageLocation, int multiplicity)
	{
		this.activated = false;
		this.description = description;
		this.duration = duration;
		this.imageLocation = imageLocation;
		this.image = null;
		this.multiplicity = multiplicity;
		this.defaultMultiplicity = multiplicity;
		this.timeRemaining = 0;
	}
	
	/**
	 * Gets the description of the powerup
	 * @return description of the powerup
	 */
	public String getDescription()
	{
		return this.description;
	}
	
	/**
	 * Gets the multiplicity of the powerup
	 * @return multiplicity of the powerup
	 */
	public int getMultiplicity()
	{
		return this.multiplicity;
	}
	
	/**
	 * Gets the default multiplicity of the powerup, i.e. how many copies of
	 * this powerup is given to the player at the start of the game.
	 * @return the default multiplicity of the powerup
	 */
	public int getDefaultMultiplicity()
	{
		return this.defaultMultiplicity;
	}
	
	/**
	 * Gets the image of the powerup
	 * @return image of the powerup
	 */
	public Image getImage() {
		return this.image;
	}
	
	/**
	 * Sets the multiplicity of the powerup
	 * @param mul the multiplicity of the powerup
	 */
	public void setMultiplicity(int mul)
	{
		this.multiplicity = mul;
	}
	
	/**
	 * Sets the Image object to be used to draw the Powerup object on the
	 * screen
	 * @param image the powerup image
	 */
	public void setImage(Image image) {
		this.image = image;
	}
	
	/**
	 * Activates the powerup
	 * @param gameState the gameplay state
	 * @return True if the powerup is activated, False otherwise
	 */
	public boolean activate(GameplayState gameState)
	{
		if (!this.activated) {
			if (this.multiplicity > 0) {
				this.activated = true;
				this.timeRemaining = this.duration;
				this.multiplicity -= 1;
				return true;
			}
		}
		return false;
	}

	/**
	 * Performs tasks when the powerup is deactivated, for example reversing
	 * the effects of the powerup.
	 * @param gameState the gameplay state
	 */
	public void deactivate(GameplayState gameState)
	{
		;
	}
	
	/**
	 * Performs the actions of a Powerup, including all internal book-keeping,
	 * provided that the powerup is not expired. If the powerup has expired,
	 * this method will deactivate it.
	 * @param gameState the gameplay state
	 * @param delta time passed since last frame (milliseconds)
	 * @return True if the powerup successfully applied its effect, False
	 * otherwise.
	 */
	public boolean update(GameplayState gameState, int delta)
	{
		if (this.activated) {
			if (this.timeRemaining > 0) {
				this.timeRemaining -= delta;
				return doUpdate(gameState);
			} else {
				this.timeRemaining = 0;
				this.activated = false;
				deactivate(gameState);
				return false;
			}
		}
		return false;
	}

	/**
	 * Performs the effects of the Powerup object.
	 * @param gameState the gameplay state
	 * @return True if the update operation is carried on successfully
	 */
	protected abstract boolean doUpdate(GameplayState gameState);
	
	/**
	 * Initializes the Powerup object ready for use in the game. This includes
	 * loading up image (if it has not been loaded yet) and resets the
	 * multiplicity of the Powerup object.
	 */
	public void init()
	{
		if (this.image == null) {
			try {
				this.image = new Image(this.imageLocation);
			} catch (SlickException e) {
				System.err.println("Cannot load powerup image");
				System.exit(1);
			}
		}
		this.multiplicity = this.defaultMultiplicity;
	}
}