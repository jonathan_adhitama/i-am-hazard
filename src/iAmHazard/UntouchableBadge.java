package iAmHazard;

import java.util.ArrayList;

/**
 * UntouchableBadge class
 * Represents the badge awarded to the player if the player finishes a
 * game without ever being damaged by a monster.
 */
public class UntouchableBadge extends Badge
{
	private static final long serialVersionUID = 2703704805560644260L;

	/**
	 * Constructor
	 * @param multiplicity the multiplicity of the badge
	 */
	public UntouchableBadge(int multiplicity)
	{
		super("Untouchable", multiplicity);
	}
	
	/**
	 * Constructor
	 */
	public UntouchableBadge()
	{
		this(1);
	}

	/**
	 * Checks if the Player is eligible to get this Badge object.
	 * @param gps GameplayState object
	 * @return True if the Player is eligible, false otherwise.
	 */
	@Override
	public boolean checkQualified(GameplayState gps)
	{
		
		if (gps.getPlayer().getHealth() == gps.getLevel().getHealth()) {
			int numberOfInstakillUsed = gps.getLevel().getPowerups().get(Constants.INSTAKILL_INDEX).getMultiplicity() 
										- gps.getPlayer().getPowerups().get(Constants.INSTAKILL_INDEX).getMultiplicity();
			if (numberOfInstakillUsed == numberOfUnansweredMonsters(gps.getMonsters())) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}
	
	/* Counts the number of unanswered questions
	 * @param monsters the list of monsters
	 * @return number of unanswered questions
	 */
	private int numberOfUnansweredMonsters(ArrayList<Monster> monsters) {
		int count = 0;
		for (Monster monster : monsters) {
			if (monster.getQuestion().getAnswerStatus() == Question.AnswerStatus.Unanswered) {
				count++;
			}
		}
		return count;
	}
}