package iAmHazard;

import org.newdawn.slick.Font;

/**
 * Question class
 * Represents a question that a Monster carries during the game.
 */
public class Question
{
	/**
	 * Denotes the status of a question, whether is is correctly answered or
	 * still left unanswered.
	 */
	public static enum AnswerStatus
	{
		Unanswered,
		Correct
	}
	
	private String question;
	private String answer;
	private AnswerStatus answerStatus;

	/**
	 * Constructor
	 * @param question the question 
	 * @param answer the answer to the question
	 */
	public Question(String question, String answer)
	{
		this.question = question;
		this.answer = answer;
		this.answerStatus = AnswerStatus.Unanswered;
	}

	/**
	 * Gets the question 
	 * @return the question
	 */
	public String getQuestion()
	{
		return this.question;
	}

	/**
	 * Gets answer to the question
	 * @return answer to the question
	 */
	public String getAnswer()
	{
		return this.answer;
	}
	
	/**
	 * Gets the status of the question
	 * @return status of the question
	 */
	public AnswerStatus getAnswerStatus()
	{
		return this.answerStatus;
	}

	/**
	 * Mark the question as answered
	 */
	public void markAnswered()
	{
		this.answerStatus = AnswerStatus.Correct;
	}
	
	/**
	 * Adds newline(s) to the question text to fit the question
	 * within a given width limit.
	 * @param f Font object used to measure the width of the text
	 * @param width the width limit to constraint the text
	 * @return the wrapped question
	 */
	public String wrappedQuestion(Font f, int width)
	{
		return this.wrap(f, width, question);
	}
	
	/**
	 * Adds newline(s) to the answer text to fit the answer
	 * within a given width limit.
	 * @param f Font object used to measure the width of the text
	 * @param width the width limit to constraint the text
	 * @return the wrapped answer
	 */
	public String wrappedAnswer(Font f, int width)
	{
		return this.wrap(f, width, answer);
	}
	
	/* Performs the wrapping routine used in wrappedQuestion() and
	 * wrappedAnswer() methods.
	 * @param f Font object used to measure the width of the text
	 * @param width the width limit to constraint the text
	 * @param text the string to be wrapped
	 * @return the wrapped string
	 */
	private String wrap(Font f, int width, String text)
	{
		String wrapped = "";
		String[] words = text.split(" ");
		boolean firstIter = true;
		for (String word : words) {
			if (firstIter) {
				// First time is a bit different
				wrapped = word;
				firstIter = false;
			} else {
				if (f.getWidth(wrapped+" "+word) > width) {
					wrapped = wrapped+"\n"+word;
				} else {
					wrapped = wrapped+" "+word;
				}
			}
		}
		return wrapped;
	}
}
