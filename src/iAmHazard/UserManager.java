package iAmHazard;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;

/**
 * UserManager class
 * Consists of static methods to manage users.
 */
public abstract class UserManager
{
	private static ArrayList<User> allUsers = null;

	/**
	 * Initialization for UserManager class 
	 */
	public static void initialize() throws FileNotFoundException, ClassNotFoundException, IOException, Exception
	{
		allUsers = loadAllUsers();
	}
	
	/**
	 * Gets the top scoring users and their score 
	 * @return the list of highscores containing the top scorers of the game
	 */
	public static ArrayList<Highscore> getHighscores()
	{
		ArrayList<Highscore> highscores = new ArrayList<Highscore>();
		Highscore tempHighscore;
		int totalLoop;
		User tempUser;
		
		Collections.sort(allUsers);
		Collections.reverse(allUsers);
		
		if (allUsers.size() >= Constants.LIMIT_HIGHSCORES) 
		{
			totalLoop = Constants.LIMIT_HIGHSCORES;
		} else {
			totalLoop = allUsers.size();
		}
		
		for (int i = 0; i < totalLoop; i++) 
		{
			tempUser = allUsers.get(i);
			tempHighscore = new Highscore (tempUser.getName(), tempUser.getHighscore());
			highscores.add(tempHighscore);
		}
		
		return highscores;
	}

	/**
	 * Tries to log in the user with the given username. If such username
	 * cannot be found from existing users data, this method will create a new
	 * user and log in with that username.
	 * @param username the username of the user to log in
	 * @return User object being logged in
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws Exception
	 */
	public static User login(String username) throws FileNotFoundException, IOException, Exception
	{
		if (allUsers == null) {
			throw new Exception ("UserManager not initialized.");
		}
		username = username.toLowerCase();
		for (User aUser: allUsers) {
			if (aUser.getName().equals(username)) {
				return aUser;
			}
		}
		
		User user = new User(username);
		allUsers.add(user);
		saveUser(user);
		return user;
	}

	/**
	 * Saves a user data type into a file.
	 * @param user the User object to be saved
	 * @throws IOException 
	 */
	public static void saveUser(User user) throws IOException
	{
		// Write to disk with FileOutputStream
		FileOutputStream f_out = new FileOutputStream(Constants.USERS_DIR + user.getName().toLowerCase() + ".data");
		
		// Write object with ObjectOutputStream
		ObjectOutputStream obj_out = new ObjectOutputStream (f_out);
		
		// Write object out to disk
		try {
			obj_out.writeObject(user);
			obj_out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/* Method of loading serialized files of the user data 
	 * @return list of all users
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws Exception
	 */
	private static ArrayList<User> loadAllUsers() throws FileNotFoundException, IOException, ClassNotFoundException, Exception
	{
		ArrayList<User> allUsers = new ArrayList<User>();
		
		// Load user file list
		File usersFolder = new File(Constants.USERS_DIR);	
		File[] usersFileList = usersFolder.listFiles();
		if (usersFileList == null) {
			throw new Exception ("Users not found.");
		}
		
		// Read each user file to allUsers 
		for (File userFile : usersFileList) {
			ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(Constants.USERS_DIR + userFile.getName()));
			try {
				Object readObject = objectInputStream.readObject();
				if (readObject instanceof User) {
					allUsers.add((User) readObject);
				}
			} catch (Exception e) {
				System.err.println("Cannot read object from "+userFile.getName()+". "+e.toString());
			} finally {
				objectInputStream.close();
			}
		}

		return allUsers;
	}
}