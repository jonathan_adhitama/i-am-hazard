package iAmHazard;

import iAmHazard.Question.AnswerStatus;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

/**
 * PerformanceSummaryState class
 * Displays a performance summary of the just completed game. This includes
 * displaying Player's score, newly achieved badges, and the list of questions
 * used throughout the game.
 */
public class PerformanceSummaryState extends BasicGameState
{
	/* General */
	private Font font;
	private StateBasedGame sbg;
	private static final float imgScale = 0.64f;
	private static final float bgScale = 0.64f;
	
	/* Performance summary background */
	private Image performanceBG;
	private static final String PERFORMANCE_BG_IMAGE = "/images/state_performance/performance_bg.png";
	private static final float PERFORMANCE_BG_IMAGE_X = 0;
	private static final float PERFORMANCE_BG_IMAGE_Y = 0;
	
	/* Performance summary bars */
	private Image performanceBars;
	private static final String PERFORMANCE_BARS_IMAGE = "/images/state_performance/performance_bars.png";
	private static final float PERFORMANCE_BARS_IMAGE_X = 0;
	private static final float PERFORMANCE_BARS_IMAGE_Y = 0;
	
	/* Buttons */
	private Image menuButton;
	private static final String MENU_BTN_IMAGE = "/images/state_performance/menu_button.png";
	private static final float MENU_BTN_IMAGE_X = 20;
	private static final float MENU_BTN_IMAGE_Y = 535;
	
	private Image replayButton;
	private static final String REPLAY_BTN_IMAGE = "/images/state_performance/replay_button.png";
	private static final float REPLAY_BTN_IMAGE_X = 295;
	private static final float REPLAY_BTN_IMAGE_Y = 535;
	
	private Image nextLevelButton;
	private static final String NEXT_LEVEL_BTN_IMAGE = "/images/state_performance/next_level_button.png";
	private static final float NEXT_LEVEL_BTN_IMAGE_X = 565;
	private static final float NEXT_LEVEL_BTN_IMAGE_Y = 535;
	
	/* Score */
	private int score;
	private static final float SCORE_X = 120;
	private static final float SCORE_Y = 115;
	
	/* Badges */
	private ArrayList<Badge> badges;
	private static final float BADGES_STR_X = 45;
	private static final float BADGES_STR_Y = 190;
	private static final float BADGES_STR_DIFF = 28;
	
	/* Questions */
	private ArrayList<Question> questions;
	private float scrollOffset;
	
	private float questionsHeight;
	private static final float TOP_MARGIN = 65;
    private static final float BOTTOM_MARGIN = 510;
    private static final float SCROLL_STOP_MARGIN = 50;
	private static final float QUESTIONS_STR_X = 280;
	private static final float QUESTIONS_STR_Y = 115;
	private static final int TAB_WIDTH = 35;
	private static final int COLUMN_WIDTH = 480;
	private static final int LINE_BREAK = 20;
	private static final float SCROLL_SPEED = 0.5f;
	private static final float QA_GAP = 5;
	private static final float Q_NEXT_GAP = 5;
	private static final float QGROUP_Q_GAP = 10;
	
	/**
	 * Gets the ID of PerformanceSummaryState
	 * @return ID of PerformanceSummaryState
	 */
	@Override
	public int getID()
	{
		return Constants.STATE_ID_PERFORMANCE;
	}
	
	/**
	 * Initializes the game state
	 * @param gc GameContainer Object
	 * @param sbg StateBasedGame Object
	 * @throws SlickException 
	 */
	@Override
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException
	{
		/* General */
		this.font = gc.getGraphics().getFont();
		this.sbg = sbg;
		
		/* Background */
		this.performanceBG = new Image(PERFORMANCE_BG_IMAGE);
		
		/* Bars */
		this.performanceBars = new Image(PERFORMANCE_BARS_IMAGE);
		
		/* Buttons */
		this.menuButton = new Image(MENU_BTN_IMAGE);
		this.replayButton = new Image(REPLAY_BTN_IMAGE);
		this.nextLevelButton = new Image(NEXT_LEVEL_BTN_IMAGE);
	}
	
	/**
	 * Performs tasks when the game state is entered
	 * @param gc GameContainer Object
	 * @param sbg StateBasedGame Object
	 */
	@Override
	public void enter(GameContainer gc, StateBasedGame sbg)
	{
		/* Score */
		this.score = GameData.getScore();
		
		/* Badges */
		this.badges = GameData.getNewBadges();
		
		/* Questions */
		this.questions = GameData.getQuestions();
		this.scrollOffset = 0;
	}

	/**
	 * Renders the game state to the screen
	 * @param gc GameContainer Object
	 * @param sbg StateBasedGame Object
	 * @param gr Graphics Object
	 */
	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics gr)
	{
		/* Background */
		this.performanceBG.draw(PERFORMANCE_BG_IMAGE_X, PERFORMANCE_BG_IMAGE_Y, bgScale);

		/* Questions */
		// Grouping the questions based on answer status
		Hashtable<AnswerStatus,ArrayList<Question>> groupedQuestion = QuestionManager.groupQuestions(this.questions);
		
		// Start printing the questions
		float offsetSoFar = 0;
		Color questionColor= new Color(Color.white);
		Enumeration<AnswerStatus> keys = groupedQuestion.keys();
		while (keys.hasMoreElements()) {
			AnswerStatus key = keys.nextElement();
			// Print the Question group name
			this.font.drawString(QUESTIONS_STR_X, QUESTIONS_STR_Y+offsetSoFar+scrollOffset, key.toString(), questionColor);
			offsetSoFar += this.font.getHeight(key.toString()) + QGROUP_Q_GAP;
			
			// Print the Question and Answer pairs in the list
			for (Question question : groupedQuestion.get(key)) {
				// Print question
				String wrappedQuestion = question.wrappedQuestion(this.font, COLUMN_WIDTH-TAB_WIDTH);
				this.font.drawString(QUESTIONS_STR_X+TAB_WIDTH, QUESTIONS_STR_Y+offsetSoFar+scrollOffset, wrappedQuestion, questionColor);
				offsetSoFar += this.font.getHeight(wrappedQuestion) + QA_GAP;
				
				// Print answer
				String wrappedAnswer = question.wrappedAnswer(this.font, COLUMN_WIDTH-2*TAB_WIDTH);
				this.font.drawString(QUESTIONS_STR_X+TAB_WIDTH*2, QUESTIONS_STR_Y+offsetSoFar+scrollOffset, wrappedAnswer, questionColor);
				offsetSoFar += this.font.getHeight(wrappedAnswer) + Q_NEXT_GAP;
			}
			
			// Add line break between question groups
			offsetSoFar += LINE_BREAK;
		}
		this.questionsHeight = offsetSoFar;
		
		/* Bars */
		this.performanceBars.draw(PERFORMANCE_BARS_IMAGE_X, PERFORMANCE_BARS_IMAGE_Y, bgScale);
		
		/* Buttons */
		this.menuButton.draw(MENU_BTN_IMAGE_X, MENU_BTN_IMAGE_Y, imgScale);
		this.replayButton.draw(REPLAY_BTN_IMAGE_X, REPLAY_BTN_IMAGE_Y, imgScale);
		if (GameData.getLevel() < Constants.LEVELS.length-1) {
			this.nextLevelButton.draw(NEXT_LEVEL_BTN_IMAGE_X, NEXT_LEVEL_BTN_IMAGE_Y, imgScale);
		}
			
		/* Score */
		this.font.drawString(SCORE_X, SCORE_Y, Integer.toString(this.score), new Color(Color.white));
		
		/* Badges */
		float badgesOffset = 0;
		for (Badge badge : this.badges) {
			this.font.drawString(BADGES_STR_X, BADGES_STR_Y+badgesOffset, badge.getName(), new Color(Color.white));
			badgesOffset += BADGES_STR_DIFF;
		}
	}

	/**
	 * Updates the game state
	 * @param gc GameContainer Object
	 * @param sbg StateBasedGame Object
	 * @param delta time passed since last frame (milliseconds).
	 */
	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta)
	{
		Input input = gc.getInput();
		float displayHeight = BOTTOM_MARGIN - TOP_MARGIN;
		if (input.isKeyDown(Input.KEY_DOWN)) {
			if (scrollOffset > displayHeight-this.questionsHeight-SCROLL_STOP_MARGIN) {
				this.scrollOffset -= SCROLL_SPEED * delta;
			}
		} else if (input.isKeyDown(Input.KEY_UP)) {
			if (scrollOffset < 0) {
				this.scrollOffset += SCROLL_SPEED * delta;
			}
		}
	}
	
	/**
	 * Handles the event when a mouse button is released (un-clicked)
	 * @param button denotes which mouse button is released
	 * @param x the x coordinate where the click was made 
	 * @param y the y coordinate where the click was made
	 */
	@Override
	public void mouseReleased(int button, int mx, int my)
	{
		if (mx >= MENU_BTN_IMAGE_X && mx <= MENU_BTN_IMAGE_X+this.menuButton.getWidth()*imgScale &&
			my >= MENU_BTN_IMAGE_Y && my <= MENU_BTN_IMAGE_Y+this.menuButton.getHeight()*imgScale) {
			/* Menu button */
			this.sbg.enterState(Constants.STATE_ID_MENU);
		} else if (mx >= REPLAY_BTN_IMAGE_X && mx <= REPLAY_BTN_IMAGE_X+this.replayButton.getWidth()*imgScale &&
				   my >= REPLAY_BTN_IMAGE_Y && my <= REPLAY_BTN_IMAGE_Y+this.replayButton.getHeight()*imgScale) {
			/* Replay button */
			this.sbg.enterState(Constants.STATE_ID_GAMEPLAY);
		} else if (mx >= NEXT_LEVEL_BTN_IMAGE_X && mx <= NEXT_LEVEL_BTN_IMAGE_X+this.nextLevelButton.getWidth()*imgScale &&
				   my >= NEXT_LEVEL_BTN_IMAGE_Y && my <= NEXT_LEVEL_BTN_IMAGE_Y+this.nextLevelButton.getHeight()*imgScale) {
			/* Next level button */
			int currentLevel = GameData.getLevel();
			if (currentLevel < Constants.LEVELS.length-1) {
				GameData.setLevel(currentLevel+1);
				this.sbg.enterState(Constants.STATE_ID_GAMEPLAY);
			}
		}
	}
}