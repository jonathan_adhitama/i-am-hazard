package iAmHazard;

import iAmHazard.Question.AnswerStatus;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * QuestionManager class
 * Consists of static methods to process raw question files to usable
 * Java objects.
 */
public class QuestionManager
{
    /**
     * Gets list of topics
     * @return available topics as Strings
     */
	public static ArrayList<String> getTopics() throws Exception
	{
		ArrayList<String> fileNames = new ArrayList<String>();
		for (File topicFile : getTopicFiles()) {
			if (topicFile.isFile()) {
				fileNames.add(topicFile.getName());
			}
		}
		return fileNames;
	}

	/**
	 * Given a topic, parses the question and answers 
	 * @param topic
	 * @throws Exception 
	 * @return list of Question objects
	 */
	public static ArrayList<Question> parseQuestions(String topic) throws Exception
	{
		ArrayList<Question> questions = new ArrayList<Question>();
		String question = null;
		String answer = null;
		
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(Constants.TOPICS_DIR+topic));
		} catch (Exception e) {
			throw new Exception ("Can't parse question for topic: " + topic);
		}
		
		try {
			while (true) {
				question = br.readLine();
				if (question == null) {
					break;
				}
				answer = br.readLine();
				if (answer == null) {
					break;
				}	
				questions.add(new Question(question, answer));
			}
		} finally {
			br.close();
		}
		
		return questions;
	}
	
	/**
	 * Group the Question objects based on their AnswerStatus value.
	 * @param questions an ArrayList of Question objects
	 * @return the grouped Question objects in the form of a Hashtable, mapping
	 * from an AnswerStatus value to an ArrayList of Question objects.
	 */
	public static Hashtable<AnswerStatus,ArrayList<Question>> groupQuestions(ArrayList<Question> questions)
	{
		Hashtable<AnswerStatus,ArrayList<Question>> groupedQuestion = new Hashtable<AnswerStatus,ArrayList<Question>>();
		for (Question question : questions) {
			AnswerStatus status = question.getAnswerStatus();
			if (groupedQuestion.containsKey(status)) {
				groupedQuestion.get(status).add(question);
			} else {
				AnswerStatus key = status;
				ArrayList<Question> value = new ArrayList<Question>();
				value.add(question);
				groupedQuestion.put(key,value);
			}
		}
		
		return groupedQuestion;
	}

	/* Gets list of topic files, each of which contains a list of questions
	 * and answers for that topic
	 * @return available topic files
	 * @throws Exception
	 */
	private static File[] getTopicFiles() throws Exception
	{
		File topicsFolder = new File(Constants.TOPICS_DIR);	
		File[] topicsFileList = topicsFolder.listFiles();
		if (topicsFileList == null) {
			throw new Exception ("Topics not found.");
		}
		return topicsFileList;
	}
}