package iAmHazard;

import java.io.Serializable;

/**
 * DeterminatorBadge class
 * Represents the badge awarded to the player if the player finishes a
 * game with exactly 1 health point remaining.
 */
public class DeterminatorBadge extends Badge implements Serializable
{
	private static final long serialVersionUID = 1809075235482564106L;

	/**
	 * Constructor
	 * @param multiplicity the multiplicity of the badge
	 */
	public DeterminatorBadge(int multiplicity)
	{
		super("Determinator", multiplicity);
	}
	
	/**
	 * Constructor
	 */
	public DeterminatorBadge()
	{
		this(1);
	}

	/**
	 * Checks if the Player is eligible to get this Badge object.
	 * @param gps GameplayState object
	 * @return True if the Player is eligible, False otherwise.
	 */
	@Override
	public boolean checkQualified(GameplayState gps)
	{
		if (gps.getPlayer().getHealth() == 1) {
			return true;
		}
		return false;
	}
}