package iAmHazard;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

/**
 * MenuState class
 * Displays an interface for the player to customize game setup before playing
 * the game.
 */
public class MenuState extends BasicGameState
{
    /* General */
    private StateBasedGame sbg;
	
	/* Image general */
	private static final float imgScale = 0.65f;
	
	/* BG */
	private Image menuBGImage;
	private static final float MENU_BG_IMAGE_X = 0;
	private static final float MENU_BG_IMAGE_Y = 0;
	private static final String MENU_BG_IMAGE_DIR = "images/state_menu/menu_bg.png";
	
	/* Topics */
	private Dictionary<String, Image> topicImages;
	private Image topicImageDisp;
	private static final float TOPIC_IMAGE_X = 95;
	private static final float TOPIC_IMAGE_Y = 190;
	private static final String[] TOPIC_NAMES = {"cell reproduction", "human body", "plants"};
	private static final String[] TOPIC_IMAGE_DIRS = {"images/state_menu/topic_cell_sel.png", "images/state_menu/topic_human_sel.png", "images/state_menu/topic_plants_sel.png"};
	private static final float[] TOPIC_BORDERS = {0.27f, 0.55f};
	private static final int TOPIC_DEFAULT_INDEX = 2;
	
	private Image levelDownImage;
	private Image levelUpImage;
	private static final float LEVEL_DOWN_IMAGE_X = 120;
	private static final float LEVEL_DOWNIMAGE_Y = 365;
	private static final float LEVEL_UP_IMAGE_X = 320;
	private static final float LEVEL_UP_IMAGE_Y = 365;
	private static final String LEVEL_DOWN_IMAGE_DIR = "images/state_menu/level_left.png";
	private static final String LEVEL_Up_IMAGE_DIR = "images/state_menu/level_right.png";
	private static final float LEVEL_STR_OFFSET_X = 195;
	private static final float LEVEL_STR_OFFSET_Y = 380;

    /* Score */
    private static final float SCORE_STR_X = 490;
    private static final float SCORE_STR_Y = 135;

    /* Badges */
    private static final float BADGES_STR_X = 535;
    private static final float BADGES_STR_Y = 230;
    private static final float BADGES_MULT_STR_X = 490;
    private static final float BADGES_MULT_STR_Y = 230;
    private static final float BADGES_STR_DIFF = 25;
	
	/* Highscores*/
    private static final float HS_NAME_STR_X = 490;
    private static final float HS_NAME_STR_Y = 365;
    private static final float HS_SCORE_STR_X = 700;
    private static final float HS_SCORE_STR_Y = 365;
    private static final float HS_STR_DIFF = 25;

    private ArrayList<Highscore> highscores;

    /* Buttons */
    private Image startBtnImage;
    private static final float START_BTN_IMAGE_X = 85;
    private static final float START_BTN_IMAGE_Y = 430;
    private static final String START_BTN_IMAGE_DIR = "images/state_menu/start_button.png";
    private Image closeBtnImage;
    private static final float CLOSE_BTN_IMAGE_X = 45;
	private static final float CLOSE_BTN_IMAGE_Y = 530;
    private static final String CLOSE_BTN_IMAGE_DIR = "images/state_menu/exit_button.png";
    private Image tutorialBtnImage;
    private static final float TUTORIAL_BTN_IMAGE_X = 245;
	private static final float TUTORIAL_BTN_IMAGE_Y = 530;
    private static final String TUTORIAL_BTN_IMAGE_DIR = "images/state_menu/tutorial_button.png";

    /* Username */
    private static final float USERNAME_STR_X = 590;
    private static final float USERNAME_STR_Y = 40;

	/**
	 * Gets the ID of MenuState
	 * @return ID of MenuState
	 */
	@Override
	public int getID()
	{
		return Constants.STATE_ID_MENU;
	}
	
    /**
	 * Initializes the game state
	 * @param gc GameContainer Object
	 * @param sbg StateBasedGame Object
	 * @throws SlickException
	 */
	@Override
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException
	{

        /* General */
        this.sbg = sbg;
		
		/* Panel */
		this.menuBGImage = new Image(MENU_BG_IMAGE_DIR);
		
		/* Topics */
		this.topicImages = new Hashtable<String, Image>();
		for (int i = 0; i < TOPIC_NAMES.length; i++) {
			this.topicImages.put(TOPIC_NAMES[i], new Image(TOPIC_IMAGE_DIRS[i]));
		}
		GameData.setTopic(TOPIC_NAMES[TOPIC_DEFAULT_INDEX]);
		this.topicImageDisp = topicImages.get(GameData.getTopic());
		
		/* Levels */
		this.levelDownImage = new Image(LEVEL_DOWN_IMAGE_DIR);
		this.levelUpImage = new Image(LEVEL_Up_IMAGE_DIR);

        /* Buttons */
        this.startBtnImage = new Image(START_BTN_IMAGE_DIR);
        this.closeBtnImage = new Image(CLOSE_BTN_IMAGE_DIR);
        this.tutorialBtnImage = new Image(TUTORIAL_BTN_IMAGE_DIR);
    }

	/**
	 * Performs tasks when the game state is entered
	 * @param gc GameContainer Object
	 * @param sbg StateBasedGame Object
	 */
    @Override
    public void enter(GameContainer gc, StateBasedGame sb) throws SlickException
    {
        super.enter(gc, sb);

        this.highscores = UserManager.getHighscores();
    }

    /**
	 * Renders the game state to the screen
	 * @param gc GameContainer Object
	 * @param sbg StateBasedGame Object
	 * @param gr Graphics Object
	 */
	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics gr)
	{
		/* Panel */
		this.menuBGImage.draw(MENU_BG_IMAGE_X, MENU_BG_IMAGE_Y, imgScale);
		
		/* Topics */
		this.topicImageDisp.draw(TOPIC_IMAGE_X, TOPIC_IMAGE_Y, imgScale);
		
		/* Levels */
		this.levelDownImage.draw(LEVEL_DOWN_IMAGE_X, LEVEL_DOWNIMAGE_Y, imgScale);
		this.levelUpImage.draw(LEVEL_UP_IMAGE_X, LEVEL_UP_IMAGE_Y, imgScale);
		gr.setColor(Color.white);
		gr.drawString(Constants.LEVELS[GameData.getLevel()].getName(), LEVEL_STR_OFFSET_X, LEVEL_STR_OFFSET_Y);
		
        /* Buttons */
        this.startBtnImage.draw(START_BTN_IMAGE_X, START_BTN_IMAGE_Y, imgScale);
        this.closeBtnImage.draw(CLOSE_BTN_IMAGE_X, CLOSE_BTN_IMAGE_Y, imgScale);
        this.tutorialBtnImage.draw(TUTORIAL_BTN_IMAGE_X, TUTORIAL_BTN_IMAGE_Y, imgScale);

        gr.setColor(Color.white);

        /* Name */
        gr.drawString(GameData.getUser().getName(), USERNAME_STR_X, USERNAME_STR_Y);

        /* Score */
        gr.drawString(Integer.toString(GameData.getUser().getHighscore()), SCORE_STR_X, SCORE_STR_Y);

        /* Badges */
        int badgesOffset = 0;
        for (Badge badge : GameData.getUser().getBadges()) {
            gr.drawString(Integer.toString(badge.getMultiplicity())+"x", BADGES_MULT_STR_X, BADGES_MULT_STR_Y+badgesOffset);
            gr.drawString(badge.getName(), BADGES_STR_X, BADGES_STR_Y+badgesOffset);
            badgesOffset += BADGES_STR_DIFF;
        }

        /* Highscores */
        int hsOffset = 0;
        for (Highscore hs : this.highscores) {
            gr.drawString(hs.getUsername(), HS_NAME_STR_X, HS_NAME_STR_Y+hsOffset);
            gr.drawString(Integer.toString(hs.getScore()), HS_SCORE_STR_X, HS_SCORE_STR_Y+hsOffset);
            hsOffset += HS_STR_DIFF;
        }
    }

	/**
	 * Updates the game state
	 * @param gc GameContainer Object
	 * @param sbg StateBasedGame Object
	 * @param delta time passed since last frame (milliseconds).
	 */
	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta)
	{

	}
	
	/**
	 * Handles the event when a mouse button is released (un-clicked)
	 * @param button denotes which mouse button is released
	 * @param x the x coordinate where the click was made 
	 * @param y the y coordinate where the click was made
	 */
	@Override
	public void mouseReleased(int button, int mx, int my) {
		/* Topics */
		if ( (mx >= TOPIC_IMAGE_X) && (mx <= (TOPIC_IMAGE_X + topicImageDisp.getWidth()*imgScale)) &&
			 (my >= TOPIC_IMAGE_Y) && (my <= (TOPIC_IMAGE_Y + topicImageDisp.getHeight()*imgScale)))
		{
			if (my <= TOPIC_IMAGE_Y + TOPIC_IMAGE_Y*imgScale*TOPIC_BORDERS[0]) {
				GameData.setTopic("plants");
			} else if (my <= TOPIC_IMAGE_Y + TOPIC_IMAGE_Y*imgScale*TOPIC_BORDERS[1]) {
				GameData.setTopic("cell reproduction");
			} else {
				GameData.setTopic("human body");
			}
		}
		topicImageDisp = topicImages.get(GameData.getTopic());
		
		/* Levels */
		if ( (mx >= LEVEL_DOWN_IMAGE_X) && (mx <= (LEVEL_DOWN_IMAGE_X + levelDownImage.getWidth()*imgScale)) &&
			 (my >= LEVEL_DOWNIMAGE_Y) && (my <= (LEVEL_DOWNIMAGE_Y + levelDownImage.getHeight()*imgScale)))
		{
			GameData.setLevel((GameData.getLevel() + 3 - 1) % Constants.LEVELS.length);
		}
        if ( (mx >= LEVEL_UP_IMAGE_X) && (mx <= (LEVEL_UP_IMAGE_X + levelUpImage.getWidth()*imgScale)) &&
             (my >= LEVEL_UP_IMAGE_Y) && (my <= (LEVEL_UP_IMAGE_Y + levelUpImage.getHeight()*imgScale)))
        {
            GameData.setLevel((GameData.getLevel() + 1) % Constants.LEVELS.length);
        }

        /* Start */
        if ( (mx >= START_BTN_IMAGE_X) && (mx <= (START_BTN_IMAGE_X + startBtnImage.getWidth()*imgScale)) &&
             (my >= START_BTN_IMAGE_Y) && (my <= (START_BTN_IMAGE_Y + startBtnImage.getHeight()*imgScale)))
        {
            this.sbg.enterState(Constants.STATE_ID_GAMEPLAY);
        }

        /* Close */
        if ( (mx >= CLOSE_BTN_IMAGE_X) && (mx <= (CLOSE_BTN_IMAGE_X + closeBtnImage.getWidth()*imgScale)) &&
             (my >= CLOSE_BTN_IMAGE_Y) && (my <= (CLOSE_BTN_IMAGE_Y + closeBtnImage.getHeight()*imgScale)))
        {
            System.exit(0);
        }
        
        /* Tutorial */
        if ( (mx >= TUTORIAL_BTN_IMAGE_X) && (mx <= (TUTORIAL_BTN_IMAGE_X + closeBtnImage.getWidth()*imgScale)) &&
             (my >= TUTORIAL_BTN_IMAGE_Y) && (my <= (TUTORIAL_BTN_IMAGE_Y + closeBtnImage.getHeight()*imgScale)))
        {
        	this.sbg.enterState(Constants.STATE_ID_TUTORIAL);
        }
	}
}