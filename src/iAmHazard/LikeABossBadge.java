package iAmHazard;

import java.util.ArrayList;

/**
 * UntouchableBadge class
 * Represents the badge awarded to the player if the player finishes a
 * game without ever being damaged by a monster.
 */
public class LikeABossBadge extends Badge
{
	private static final long serialVersionUID = -2376283315598874756L;

	/**
	 * Constructor
	 * @param multiplicity the multiplicity of the badge
	 */
	public LikeABossBadge(int multiplicity)
	{
		super("Like A Boss!", multiplicity);
	}
	
	/**
	 * Constructor
	 */
	public LikeABossBadge()
	{
		this(1);
	}

	/**
	 * Checks if the Player is eligible to get this Badge object.
	 * @param gps GameplayState object
	 * @return True if the Player is eligible, false otherwise.
	 */
	@Override
	public boolean checkQualified(GameplayState gps)
	{
		
		if (gps.getPlayer().getHealth() == gps.getLevel().getHealth()
			&& checkAllPowerups(gps.getPlayer().getPowerups())) {
			return true;
		}
		return false;
	}
	
	/* Checks whether the player has used a powerup at all
	 * @param powerups the list of powerups
	 * @return True if the player has not used a powerup at all, False otherwise
	 */
	private boolean checkAllPowerups(ArrayList<Powerup> powerups) {
		for (Powerup powerup : powerups) {
			if (powerup.getMultiplicity() != powerup.getDefaultMultiplicity()) {
				return false;
			}
		}
		return true;
	}
}