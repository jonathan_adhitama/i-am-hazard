package iAmHazard;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

/**
 * GameObject (abstract) class
 * Represents a game object inside GameplayState
 */
public abstract class GameObject
{
	private boolean exists;
	private Image image;
	private float xposition;
	private float yposition;
	private float toXposition;
	private float toYposition;
	private float speed;
	
	/**
	 * Constructor
	 * @param xposition the x position of the game object
	 * @param yposition the y position of the game object
	 * @param image the Image object to represent the game object graphically
	 * on the screen
	 */
	public GameObject(float xposition, float yposition, Image image, float speed)
	{
		this.xposition = xposition;
		this.yposition = yposition;
		this.toXposition = xposition;
		this.toYposition = yposition;
		this.image = image;
		this.exists = true;
		this.speed = speed;
	}
	
	/**
	 * Gets the image of the game object
	 * @return the image of the game object
	 */
	public Image getImage()
	{
		return this.image;
	}

	/**
	 * Gets the x position of the game object
	 * @return the x position of the game object
	 */
	public float getXposition()
	{
		return this.xposition;
	}
	
	/**
	 * Gets the y position of the game object
	 * @return the y position of the game object
	 */
	public float getYposition()
	{
		return this.yposition;
	}
	
	/**
	 * Gets the x position the game object is moving to
	 * @return the x position the game object is moving to
	 */
	public float getToXposition()
	{
		return this.toXposition;
	}

	/**
	 * Gets the y position the game object is moving to
	 * @return the y position the game object is moving to
	 */
	public float getToYposition()
	{
		return this.toYposition;
	}
	
	/**
	 * Gets the speed at which the game object is moving
	 * @return the speed at which the game object is moving
	 */
	public float getSpeed()
	{
		return this.speed;
	}

	/**
	 * Gets the status of whether the game object should be rendered on screen
	 * @return True if the game object should be rendered on screen, False otherwise
	 */
	public boolean isExistent()
	{
		return this.exists;
	}

	/**
	 * Sets the status of whether the game object should be rendered on screen
	 * @param boolean value to denote whether the game object should be
	 * rendered or not
	 */
	public void setExistent(boolean bool)
	{
		this.exists = bool;
	}
	
	/**
	 * Sets the x position of the game object
	 * @param xposition the x position of the game object
	 */
	public void setXposition(float xposition)
	{
		this.xposition = xposition;
	}
	
	/**
	 * Sets the y position of the game object
	 * @param yposition the y position of the game object
	 */
	public void setYposition(float yposition)
	{
		this.yposition = yposition;
	}
	
	/**
	 * Sets the x position the game object is to be moved to
	 * @param xposition the new x position
	 */
	public void setToXposition(float xposition)
	{
		this.toXposition = xposition;
	}
	
	/**
	 * Sets the y position the game object is to be moved to
	 * @param yposition the new y position
	 */
	public void setToYposition(float yposition)
	{
		this.toYposition = yposition;
	}
	
	/**
	 * Sets the speed at which the game object is moving
	 * @param speed the new speed
	 */
	public void setSpeed(float speed)
	{
		this.speed = speed;
	}

	/**
	 * Renders the GameObject on the screen as necessary.
	 * If the GameObject is out of the screen, it will not be rendered.
	 * @param gr Graphics object
	 * @param imgScale the scale to which the game object image should be
	 * rendered
	 */
	public void draw(Graphics gr, float imgScale)
	{
        if (!this.isExistent()) {
            return;
        }
		// Checks if the GameObject is within the screen
		int screenWidth = Constants.SCREENWIDTH;
		int screenHeight = Constants.SCREENHEIGHT;
		boolean isOnScreen = (this.xposition > 0-this.image.getWidth()) &&
				             (this.yposition > 0-this.image.getHeight()) &&
				             (this.xposition < screenWidth+this.image.getWidth()) &&
				             (this.yposition < screenHeight+this.image.getHeight());
		
		// Draws the GameObject as necessary
		if (isOnScreen) {
			this.image.draw(this.xposition - this.image.getWidth()*imgScale/2,
                            this.yposition - this.image.getHeight()*imgScale/2,
                            imgScale);
		}
	}
	
	/**
	 * Moves the GameObject to the given x and y positions.
	 * @param xpos x position to move the GameObject to
	 * @param ypos y position to move the GameObject to
	 * @return True if the GameObject is to be moved, False otherwise.
	 */
	public boolean move(float xpos, float ypos)
	{
		if (this.getXposition() != xpos || this.getYposition() != ypos) {
			this.setToXposition(xpos);
			this.setToYposition(ypos);
			return true;
		}
		return false;
	}
	
	/**
	 * Updates the GameObject's position to the destination position, provided that
	 * the GameObject is not already there.
	 * @param delta time passed since last frame (milliseconds)
	 * @return True if the GameObject is moved, False otherwise
	 */
	public boolean updatePosition(int delta)
	{
		if (this.isMoving()) {
			// Updating x position
			float toXpos = this.getToXposition();
			float oldXpos = this.getXposition();
			if (toXpos != oldXpos) {
				int oldXDirection = toXpos-oldXpos > 0 ? 1 : -1;
				float newXpos = oldXDirection*this.getSpeed()*delta + oldXpos;
				int newXDirection = toXpos-newXpos > 0 ? 1 : -1;
				if (newXDirection == oldXDirection) {
					this.setXposition(newXpos);
				} else {
					this.setXposition(toXpos);
				}
			}
			
			// Updating y position
			float toYpos = this.getToYposition();
			float oldYpos = this.getYposition();
			if (toYpos != oldYpos) {
				int oldYDirection = toYpos-oldYpos > 0 ? 1 : -1;
				float newYpos = oldYDirection*this.getSpeed()*delta + oldYpos;
				int newYDirection = toYpos-newYpos > 0 ? 1 : -1;
				if (newYDirection == oldYDirection) {
					this.setYposition(newYpos);
				} else {
					this.setYposition(toYpos);
				}
			}
			return true;
		}
		return false;
	}
	
	/**
	 * Denotes if the GameObject is still moving on the screen.
	 * @return True if the GameObject is still moving
	 */
	public boolean isMoving()
	{
		return (this.getToXposition() != this.getXposition()) ||
			   (this.getToYposition() != this.getYposition());
	}
}